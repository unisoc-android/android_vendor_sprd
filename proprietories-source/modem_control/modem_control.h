/**
 * modem_control.h ---
 *
 * Copyright (C) 2015-2018 Spreadtrum Communications Inc.
 */

#ifndef MODEM_CONTROL_H_
#define MODEM_CONTROL_H_
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <log/log.h>

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "MODEM_CTRL"
#endif

// #define MODEM_DEBUG

#ifdef MODEM_DEBUG
#define MODEM_LOGIF ALOGD
#else
#define MODEM_LOGIF(...)
#endif

#define MODEM_LOGE ALOGE
#define MODEM_LOGD ALOGD

#define min(A, B) (((A) < (B)) ? (A) : (B))
#define MODEM_SUCC (0)
#define MODEM_ERR (-1)

#define CPCMDLINE_SIZE (0x1000)

#define TD_MODEM 0x3434
#define W_MODEM 0x5656
#define LTE_MODEM 0x7878
#define NR_MODEM 0x9A9A

#define WDOG_BANK "wdtirq"
#define MAX_ASSERT_INFO_LEN 256

#define MINIAP_PANIC "Miniap Panic"
#define MODEM_ALIVE "Modem Alive"
#define PREPARE_RESET "Prepare Reset"
#define MODEM_RESET "Modem Reset"
#define MODEM_ASSERT "Modem Assert"

#define DSP_HUNG "HUNG"

#define MODEM_RADIO_TYPE "ro.vendor.radio.modemtype"
#define ASSERT_DEV_PROP "ro.vendor.modem.assert"
#define DIAG_DEV_PROP "ro.vendor.modem.diag"
#define LOG_DEV_PROP "ro.vendor.modem.log"
#define ALIVE_DEV_PROP "ro.vendor.modem.alive"

#define TTY_DEV_PROP "ro.vendor.modem.tty"
#define PROC_DEV_PROP "ro.vendor.modem.dev"
#define MODEM_RESET_PROP "persist.vendor.sys.modemreset"

#define PMIC_MONITOR_PATH "/dev/sctl_pm"

enum {
  BOOT_MODE_NORMAL = 0,
  BOOT_MODE_CALIBRATION,
  BOOT_MODE_FACTORY,
  BOOT_MODE_RECOVERY
};


int modem_ctrl_int_modem_type(void);
int modem_ctrl_get_modem_type(void);
void* modem_ctrl_listen_modem(void *param);
void *modem_ctrl_listen_sp(void *param);
void modem_ctrl_enable_wake_lock(bool bEnable, const char *pos);
void modem_ctrl_enable_busmonitor(bool bEnable);
void modem_ctrl_enable_dmc_mpu(bool bEnable);
int modem_ctrl_parse_cmdline(char *cmdvalue);
int modem_has_been_boot(void);
bool wait_for_modem_alive(void);
int modem_ctrl_boot_modem (void) ;
void *modem_ctrl_listen_clients(void *param);
void modem_ctrl_set_wait_reset_flag(int flag);
void modem_ctrl_reboot_all_system(void);
int modem_ctrl_get_boot_mode(void);

#ifdef FEATURE_EXTERNAL_MODEM
void modem_event_init(void);
#ifdef FEATURE_PCIE_RESCAN
void modem_pcie_init(void);
#endif
void modem_ctrl_set_miniap_panic(int panic);
#endif

#endif  // MODEM_CONTROL_H_

