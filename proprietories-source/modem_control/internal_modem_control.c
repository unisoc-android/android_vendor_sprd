
/**
 * internal_modem_control.c ---
 *
 * Copyright (C) 2018 Spreadtrum Communications Inc.
 */
#include <cutils/properties.h>

#include "modem_control.h"
#include "modem_load.h"
#include "modem_connect.h"
#include <pthread.h>

static int assert_fd = -1;

void modem_ctrl_reboot_all_system(void) {
  sleep(1);

  system("echo \"c\" > /proc/sysrq-trigger");

  while(1)
    sleep(1);
}

/* loop detect sipc modem state */
void *modem_ctrl_listen_modem(void *param) {
  char assert_dev[PROPERTY_VALUE_MAX] = {0};
  char watchdog_dev[PROPERTY_VALUE_MAX] = {0};
  int ret, watchdog_fd, max_fd, fd = -1;
  fd_set rfds;
  char buf[256];
  int numRead, numWrite;
  pthread_t t1;

  MODEM_LOGD("Enter %s !", __FUNCTION__);

  /* get diag assert prop */
  property_get(ASSERT_DEV_PROP, assert_dev, "not_find");
  MODEM_LOGD("%s: %s = %s\n", __FUNCTION__, ASSERT_DEV_PROP, assert_dev);

  /* get watchdog dev */
  property_get(ASSERT_DEV_PROP, watchdog_dev, "not_find");
  strncat(watchdog_dev, WDOG_BANK,
          sizeof(watchdog_dev) - strlen(watchdog_dev) - 1);
  MODEM_LOGD("%s: %s = %s\n", __FUNCTION__, watchdog_dev, watchdog_dev);

  pthread_t tid;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  /*set up socket connection to clients*/
  if (pthread_create(&tid, &attr, (void*)modem_setup_clients_connect, NULL) < 0) {
      MODEM_LOGE("Failed to create modemd listen accept thread");
  } else {
      /* create listen_thread */
      if (0 != pthread_create(&t1, NULL, modem_ctrl_listen_clients, NULL)) {
        MODEM_LOGE(" %s: create error!\n", __FUNCTION__);
      }
  }

  /* open assert_dev and watchdog_dev */
  while ((assert_fd = open(assert_dev, O_RDWR)) < 0) {
    MODEM_LOGE("%s: open %s failed, error: %s\n", __FUNCTION__, assert_dev,
               strerror(errno));
    sleep(1);
  }
  MODEM_LOGD("%s: open assert dev: %s, fd = %d\n", __FUNCTION__, assert_dev,
             assert_fd);
  watchdog_fd = open(watchdog_dev, O_RDONLY);
  MODEM_LOGD("%s: open watchdog dev: %s, fd = %d", __FUNCTION__, watchdog_dev,
             watchdog_fd);
  if (watchdog_fd < 0) {
    /* if  watchdog_fd can't be open, we also continue */
    MODEM_LOGD("open %s unsupport watchdog, errno: %s",
               watchdog_dev, strerror(errno));
  }
  max_fd = watchdog_fd > assert_fd ? watchdog_fd : assert_fd;

  FD_ZERO(&rfds);
  FD_SET(assert_fd, &rfds);
  if (watchdog_fd >= 0) {
    FD_SET(watchdog_fd, &rfds);
  }

  /*listen assert and WDG event*/
  for (;;) {
    MODEM_LOGD("%s: wait for modem assert/hangup event ...", __FUNCTION__);
    do {
      ret = select(max_fd + 1, &rfds, NULL, NULL, 0);
    } while (ret == -1 && errno == EINTR);
    if (ret > 0) {
      if (FD_ISSET(assert_fd, &rfds)) {
        fd = assert_fd;
      } else if (watchdog_fd >= 0 && FD_ISSET(watchdog_fd, &rfds)) {
        fd = watchdog_fd;
      } else {
        MODEM_LOGD("%s: none of assert and watchdog fd is readalbe",
                   __FUNCTION__);
        sleep(1);
        continue;
      }

	  /* get wake_lock */
      modem_ctrl_enable_wake_lock(1, __FUNCTION__);
      memset(buf, 0, sizeof(buf));
      MODEM_LOGD("%s: enter read ...", __FUNCTION__);
      numRead = read(fd, buf, sizeof(buf) -1);
      if (numRead <= 0) {
        MODEM_LOGE("%s: read %d return %d, errno = %s", __FUNCTION__, fd,
                   numRead, strerror(errno));
        sleep(1);
        continue;
      }

      MODEM_LOGD("%s: buf=%s", __FUNCTION__, buf);

      if (strstr(buf, "Modem Reset")) {
          MODEM_LOGD("read Modem Reset from modem\n");
          modem_ctrl_set_wait_reset_flag(0);
      }

      /*if cp or dsp hung, if mode reset open, will reboot system */
      if (strstr(buf, "HUNG")) {
          char prop[PROPERTY_VALUE_MAX];
          int is_reset;

          memset(prop, 0, sizeof(prop));
          property_get(MODEM_RESET_PROP, prop, "0");
          is_reset = atoi(prop);
          MODEM_LOGD("%s = %s, is reset = %d\n",
                     MODEM_RESET_PROP, prop, is_reset);

          if (is_reset) {
              MODEM_LOGD("%s: dsp HUNG, reboot system\n", __FUNCTION__);
              modem_ctrl_reboot_all_system();
          }
      }

      if (modem_ctrl_get_boot_mode() != BOOT_MODE_CALIBRATION) {
        numWrite = modem_write_data_to_clients(buf, numRead);
        MODEM_LOGD("%s: write to modemd len = %d\n", __FUNCTION__, numWrite);
	  }
      /* release wake_lock */
      modem_ctrl_enable_wake_lock(0, __FUNCTION__);
    }
  }
  return NULL;
}

