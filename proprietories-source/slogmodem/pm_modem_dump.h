/*
 *  pm_modem_dump.h - The cellular MODEM and PM Sensorhub dump class.
 *
 *  Copyright (C) 2015-2017 Spreadtrum Communications Inc.
 *
 *  History:
 *
 *  2015-6-15 Zhang Ziyi
 *  Initial version.
 *
 *  2016-7-13 Yan Zhihang
 *  Dump consumer to support both modem and pm sensorhub.
 *
 *  2017-6-3 Zhang Ziyi
 *  Join the Transaction class family.
 */

#ifndef _PM_MODEM_DUMP_H_
#define _PM_MODEM_DUMP_H_

#include "cp_dump.h"
#include "diag_stream_parser.h"
#include "timer_mgr.h"

class LogFile;

class TransPmModemDump : public TransCpDump {
 public:
  using DumpOngoingCallback = void (*)(void*);

  TransPmModemDump(LogPipeHandler* subsys, CpStorage& cp_stor,
                   const struct tm& lt, const char* dump_path,
                   const char* prefix,
                   const char* end_of_content, uint8_t cmd);
  TransPmModemDump(const TransPmModemDump&) = delete;
  ~TransPmModemDump();

  TransPmModemDump& operator = (const TransPmModemDump&) = delete;

  // Transaction::execute()
  int execute() override;
  // Transaction::cancel()
  void cancel() override;

  // TransDiagDevice::process()
  bool process(DataBuffer& buffer) override;

  // Register ongoing periodic event
  void set_periodic_callback(DumpOngoingCallback cb) {
    on_going_cb_ = cb;
  }

 private:
  int start_dump();

  bool check_ending(DataBuffer& buffer);

  /* save_dump_file - if reading dump from spipe failed, save /proc/cpxxx/mem
   *                  save /proc/cptl/aon-iram and /proc/cptl/pubcp-iram
   *                  if supported
   */
  bool save_dump_file();

  /* copy_dump_content - copy the memory to dump file
   * @mem_name : file name construction
   * @mem_path : dump mem path
   */
  bool copy_dump_content(const char* mem_name, const char* mem_path);

  /*  notify_stor_inactive - callback when external storage umounted.
   */
  static void notify_stor_inactive(void* client, unsigned priority);

  static void dump_read_check(void* param);

 private:
  const char* m_dump_path;
  TimerManager::Timer* m_timer;
  DiagStreamParser parser_;
  const char* m_end_of_content;
  size_t m_end_content_size;
  const uint8_t m_cmd;
  // Whether data are received in the most recent time window
  bool dump_ongoing_;
  // The number of adjacent data silent windows
  int silent_wins_;
  DumpOngoingCallback on_going_cb_;
};

#endif  // !_PM_MODEM_DUMP_H_
