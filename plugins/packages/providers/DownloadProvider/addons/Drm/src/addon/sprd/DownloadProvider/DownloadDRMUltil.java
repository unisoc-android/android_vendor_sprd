/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package addon.sprd.downloadprovider;

import static android.provider.Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI;

import static android.app.DownloadManager.COLUMN_URI;
import java.io.File;
import android.R.bool;
import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.app.DownloadManager.Query;
import android.app.FragmentManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.provider.Downloads.Impl.RequestHeaders;
import android.database.Cursor;
import android.provider.DocumentsContract;
import android.drm.DrmManagerClient;
import android.drm.DrmStore;
import android.drm.DrmStore.DrmObjectType;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.android.providers.downloads.Constants;
import com.android.providers.downloads.OpenHelper;
import libcore.io.IoUtils;
import android.os.SystemProperties;
import android.os.Process;
import android.drm.DrmStore.RightsStatus;
import addon.sprd.downloadprovider.DownloadDrmHelper;
import addon.sprd.downloadprovider.DownloadDrmHelper.DrmClientOnErrorListener;
import static android.app.DownloadManager.COLUMN_MEDIA_TYPE;

/**
 * Intercept all download clicks to provide special behavior. For example,
 * PackageInstaller really wants raw file paths.
 */
public class DownloadDRMUltil {
    private static final String TAG = "DownloadProviderHelper";
    public static final int SHOW_DOwnLOADS = 1;
    public static final int SHOW_CONSUME_DIALOG = 2;
    public static final int SHOW_OPEN_FILE = 3;
    public static final int SHOW_RENEW_DRM_FILE = 4;

    public static boolean isShowDrmConsumeDilog(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimeType);
        if (isDrm){
            int drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
            if (drmType == DrmStore.DrmObjectType.CONTENT){
                int rightsStates = DownloadDrmHelper.getRightsStatus(context, filename, mimeType);
                if (rightsStates == RightsStatus.RIGHTS_VALID){
                    if (DownloadDrmHelper.isDrmFLFile(context, filename, mimeType)){
                        return false;
                    }else{
                        return true;
                    }
                }
            }
        }else{
            return false;
        }
        return false;
    }

    public static boolean isDrmCDFile(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimeType);
        if (isDrm){
            int drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
            if (drmType == DrmStore.DrmObjectType.CONTENT){
                int rightsStates = DownloadDrmHelper.getRightsStatus(context, filename, mimeType);
                if (rightsStates == RightsStatus.RIGHTS_VALID){
                    return false;
                }else{
                    if (DownloadDrmHelper.isDrmCDFile(context, filename, mimeType)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isShowDrmRenewDilog(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename,mimeType);
        Log.d(TAG, "isDrm --  "+isDrm);
        if (isDrm){
            int drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
//            final String originalMimeType = DownloadDrmHelper.getOriginalMimeType(c, drmPath, mimeType);
            //mimeType = originalMimeType;
            Log.d(TAG, "drmType --  " + drmType + " DrmStore.DrmObjectType.CONTENT = " + DrmStore.DrmObjectType.CONTENT);
            if (drmType == DrmStore.DrmObjectType.CONTENT){

                Log.d(TAG, "isShowDrmRenewDilog --  DrmStore.DrmObjectType.CONTENT =" + DrmStore.DrmObjectType.CONTENT);
                int rightsStates = DownloadDrmHelper.getRightsStatus(context, filename, mimeType);
                Log.d(TAG, "isShowDrmRenewDilog --rightsStates ="+rightsStates + "RightsStatus.RIGHTS_VALID = " +RightsStatus.RIGHTS_VALID);
                if (rightsStates == RightsStatus.RIGHTS_VALID){
                    return false;
                }else{
                    return true;
                }
            }
        }else{
            return false;
        }
        return false;
    }

    public static boolean isShowDrmInstallDilog(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimeType);
        Log.d(TAG, "isDrm --  " + isDrm + "filename:" + filename + "mimeType" + mimeType);
        if (isDrm){
            int drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
            if (drmType == DrmStore.DrmObjectType.CONTENT){
                return false;
            }else if (drmType == DrmStore.DrmObjectType.RIGHTS_OBJECT){
                return true;
            }else if (drmType == DrmStore.DrmObjectType.TRIGGER_OBJECT){
                return true;
            }
        }else{
            return false;
        }
        return false;
    }

    public static int getInstallDrmType(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimeType);
        int drmType = -1;
        Log.d(TAG, "isDrm" + isDrm + "filename:" + filename + "mimeType" + mimeType);
        if (isDrm){
             drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
        }
        Log.d(TAG, "drmType" + drmType);
        return drmType;
    }

    public static String getOriginalMimeType(Context context,String filename, String mimeType){
        String type = mimeType;
        Log.d(TAG, "getOriginalMimeType type" + type);
        if (filename != null){
            type = DownloadDrmHelper.getOriginalMimeType(context, filename, mimeType);
        }
        Log.d(TAG, "getOriginalMimeType after type" + type);
        return type;
    }

    public static boolean isDrmType(Context context, String filename, String mimetype){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimetype);
        Log.d(TAG, "isDrmType isDrm = " + isDrm);
        return isDrm;
    }

    public static boolean isSupportDRMType(Context context,String filename,String mimetype){
        String type = mimetype;
        type = DownloadDrmHelper.getOriginalMimeType(context, filename, mimetype);
        Log.d(TAG, "isSupportDRMType type = " + type);
        if (type != null){
              if (type.startsWith("video/") || type.startsWith("image/")
                      || type.startsWith("audio/") || type.equalsIgnoreCase("application/ogg")){
                    return true;
             }
        }
        return false;
    }

  //add for drm
    public static boolean isDrmEnabled() {
        String prop = SystemProperties.get("drm.service.enabled");
        Log.d(TAG, "isDrmEnabled type = " + prop);
        return prop != null && prop.equals("true");
    }

    public static int getRightsIntoDownloads(Context context, String filename, String mimetype){
        Log.d(TAG, "getRightsIntoDownloads:" + filename + "mimetype:" + mimetype);
        if (DownloadDrmHelper.isDrmMimeType(context, filename, mimetype)){
            if (!DownloadDrmHelper.isSupportDRMType(context, filename, mimetype)){
                return SHOW_DOwnLOADS;
            }
            if (filename != null && filename.endsWith(".dm")){
                return SHOW_DOwnLOADS;
            }
            if (mimetype.equals(DownloadDrmHelper.DRM_CONTENT_TYPE)){
                int rightsStates = DownloadDrmHelper.getRightsStatus(context, filename, mimetype);
                if (rightsStates == RightsStatus.RIGHTS_VALID){
                    if (!DownloadDrmHelper.isDrmFLFile(context, filename, mimetype)){
                        return SHOW_CONSUME_DIALOG;
                    }else{
                        return SHOW_OPEN_FILE;
                    }
                }else {
                    return SHOW_RENEW_DRM_FILE;
                }
            }else{
                return SHOW_DOwnLOADS;
            }
        }
        return SHOW_OPEN_FILE;
    }

    public static Intent startPluginViewIntent(Context context, long id, int intentFlags){
        final DownloadManager downManager = (DownloadManager) context.getSystemService(
                Context.DOWNLOAD_SERVICE);
        downManager.setAccessAllDownloads(true);
        downManager.setAccessFilename(true);

        final Cursor cursor = downManager.query(new DownloadManager.Query().setFilterById(id));
        try {
            if (!cursor.moveToFirst()) {
                return null;
            }

            final Uri localUri = getCursorUri(cursor, DownloadManager.COLUMN_LOCAL_URI);
            final File file = getCursorFile(cursor, DownloadManager.COLUMN_LOCAL_FILENAME);

            String originalMimeType = getCursorString(cursor, DownloadManager.COLUMN_MEDIA_TYPE);
            if ((file != null) && (!file.exists())){
                return null;
            }
            String mimeType = getDRMPluginMimeType(context, file, originalMimeType, cursor);
            if ((mimeType != null) && (mimeType.equals("image/jpg"))){
                mimeType = "image/jpeg";
            }
            //modify for flv video support in GMS version
            if ((mimeType != null) && ((mimeType.equals("video/flv") || mimeType.equals("video/x-flv")))){
                mimeType = "video/*";
            }
            final Uri documentUri = DocumentsContract.buildDocumentUri(
                    Constants.STORAGE_AUTHORITY, String.valueOf(id));
            final Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.putExtra("drmpath", file.getPath());
            intent.putExtra("drmtype", originalMimeType);
            /*@}*/
            if ("application/vnd.android.package-archive".equals(mimeType) || "application/vnd.android".equals(mimeType)) {
                // PackageInstaller doesn't like content URIs, so open file
                mimeType = "application/vnd.android.package-archive";
                intent.setDataAndType(localUri, mimeType);

                // Also splice in details about where it came from
                final Uri remoteUri = getCursorUri(cursor, COLUMN_URI);
                intent.putExtra(Intent.EXTRA_ORIGINATING_URI, remoteUri);
                intent.putExtra(Intent.EXTRA_REFERRER, getRefererUri(context, id));
                intent.putExtra(Intent.EXTRA_ORIGINATING_UID, getOriginatingUid(context, id));

            } else if ("application/vnd.oma.drm.content".equals(originalMimeType)) {
                intent.setDataAndType(localUri, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            } else {
                intent.setDataAndType(documentUri, mimeType);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            intent.addFlags(intentFlags);
            return intent;
        } finally {
            cursor.close();
        }
    }

    public static String getDRMPluginMimeType(Context context, File file, String mimeType, Cursor cursor) {
        Log.d(TAG, "getDRMPluginMimeType:mimeType =  " + mimeType);
        return cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MEDIA_TYPE));
    }

    public static boolean isDrmFLFileInValid(Context context, String filename, String mimeType){
        boolean isDrm = DownloadDrmHelper.isDrmMimeType(context, filename, mimeType);
        Log.d(TAG, "isDrmFLFileInValid isDrm--- " + isDrm);
        if (isDrm){
            int drmType = DownloadDrmHelper.getDrmObjectType(context, filename, mimeType);
            Log.d(TAG, "isDrmFLFileInValid drmType--- " + drmType);
            if (drmType == DrmStore.DrmObjectType.CONTENT){
                int rightsStates = DownloadDrmHelper.getRightsStatus(context, filename, mimeType);
                Log.d(TAG, "isDrmFLFileInValid rightsStates--- " + rightsStates);
                if (rightsStates == RightsStatus.RIGHTS_VALID){
                    Log.d(TAG, "isDrmFLFileInValid rightsStates--- " + rightsStates);
                    return false;
                }else{
                    if (DownloadDrmHelper.isDrmFLFile(context, filename, mimeType)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static int getOriginatingUid(Context context, long id) {
        final Uri uri = ContentUris.withAppendedId(ALL_DOWNLOADS_CONTENT_URI, id);
        final Cursor cursor = context.getContentResolver().query(uri, new String[]{Constants.UID},
                null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    final int uid = cursor.getInt(cursor.getColumnIndexOrThrow(Constants.UID));
                    if (uid != Process.myUid()) {
                        return uid;
                    }
                }
            } finally {
                cursor.close();
            }
        }
        return PackageInstaller.SessionParams.UID_UNKNOWN;
    }

    private static Uri getRefererUri(Context context, long id) {
        final Uri headersUri = Uri.withAppendedPath(
                ContentUris.withAppendedId(ALL_DOWNLOADS_CONTENT_URI, id),
                RequestHeaders.URI_SEGMENT);
        final Cursor headers = context.getContentResolver()
                .query(headersUri, null, null, null, null);
        try {
            while (headers.moveToNext()) {
                final String header = getCursorString(headers, RequestHeaders.COLUMN_HEADER);
                if ("Referer".equalsIgnoreCase(header)) {
                    return getCursorUri(headers, RequestHeaders.COLUMN_VALUE);
                }
            }
        } finally {
            headers.close();
        }
        return null;
    }

    private static File getCursorFile(Cursor cursor, String column) {
        return new File(cursor.getString(cursor.getColumnIndexOrThrow(column)));
    }

    private static String getCursorString(Cursor cursor, String column) {
        return cursor.getString(cursor.getColumnIndexOrThrow(column));
    }

    private static Uri getCursorUri(Cursor cursor, String column) {
        final String uriString = cursor.getString(cursor.getColumnIndexOrThrow(column));
        return uriString == null ? null : Uri.parse(uriString);
    }
}
