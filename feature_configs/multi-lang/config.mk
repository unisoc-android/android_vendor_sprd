# The Spreadtrum Communications Inc. 2015

# This is the top of this features, this feature may
# contains much sub-features, and they may divided
# and take effects by properties / addons / overlays or
# prebuilts

# You can gather them by build vars as follows:

# properties -> FEATURES.PRODUCT_PROPERTY_OVERRIDES +=
# addons/packages/prebuilts -> FEATURES.PRODUCT_PACKAGES +=
# overlay -> FEATURES.PRODUCT_PACKAGE_OVERLAYS +=
#

# Now, let's get it on as follows

FEATURES.PRODUCT_LOCALES := en_US zh_CN zh_HK zh_TW ar_EG fa_IR ru_RU fr_FR sw_TZ th_TH tr_TR es_ES es_US hi_IN in_ID vi_VN my_MM uk_UA pt_PT pt_BR as_ET ms_MY bn_BD tl_PH te_IN ta_IN ur_PK am_ET de_DE el_GR ml_IN mr_IN kn_IN hu_HU sq_AL fi_FI ca_ES eu_ES gl_ES km_KH lo_LA ne_NP si_LK or_IN pa_IN nl_NL it_IT

FEATURES.PRODUCT_PACKAGE_OVERLAYS += \
    $(PRODUCT_REVISION_COMMON_CONFIG_PATH)/multi-lang/overlay
