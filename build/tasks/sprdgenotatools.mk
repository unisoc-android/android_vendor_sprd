LOCAL_PATH := $(call my-dir)


LOCAL_NVMERGE_INTERMEDIATES_FILE := $(call intermediates-dir-for,EXECUTABLES,nvmerge,,,$(TARGET_PREFER_32_BIT))/nvmerge
LOCAL_UPDATER_INTERMEDIATES_FILE := $(call intermediates-dir-for,EXECUTABLES,updater,,,$(TARGET_PREFER_32_BIT))/updater
LOCAL_MKF2FS_INTERMEDIATES_FILE := $(TARGET_RECOVERY_ROOT_OUT)/system/bin/make_f2fs
LOCAL_SLOADF2FS_INTERMEDIATES_FILE := $(TARGET_RECOVERY_ROOT_OUT)/system/bin/sload_f2fs

OTA_BINS := $(LOCAL_NVMERGE_INTERMEDIATES_FILE) \
	$(LOCAL_UPDATER_INTERMEDIATES_FILE) \
	$(OTA_PTOQ_REPART_BIN) \
	$(LOCAL_MKF2FS_INTERMEDIATES_FILE) \
	$(LOCAL_SLOADF2FS_INTERMEDIATES_FILE) \
	$(TARGET_RECOVERY_NVMERGE_CONFIG) \
	$(MODEM_UPDATE_CONFIG_FILE)


$(OTATOOLS): $(OTA_BINS) 

#-------------------------------------------------------------------------------------------------
SPRD_OTA_TOOLS_BASE := ./vendor/sprd/tools/ota/otatool
SPRD_OTA_TOOLS := $(PRODUCT_OUT)/otatools
SPRD_OTA_BINS := $(SPRD_OTA_TOOLS)/device/sprd/$(PROPRIETARY_BOARD)/ota_bin
SPRD_OTA_PREBUILD_BINS := $(SPRD_OTA_TOOLS)/prebuilts/linux-x86/ota/bin
SPRD_OTA_PREBUILD_SIGNAPK := $(SPRD_OTA_TOOLS)/prebuilts/linux-x86/ota/framework
SPRD_OTA_PREBUILD_LIB64 := $(SPRD_OTA_TOOLS)/prebuilts/linux-x86/ota/lib64

$(SPRD_OTA_TOOLS): $(OTATOOLS) $(SPRD_OTA_TOOLS_BASE)
	$(hide) echo "Build SPRD otatools......."

	$(hide) mkdir -p $(SPRD_OTA_TOOLS)
	$(hide) mkdir -p $(SPRD_OTA_PREBUILD_SIGNAPK)
	$(hide) mkdir -p $(SPRD_OTA_PREBUILD_LIB64)
	$(hide) cp -rpf $(SPRD_OTA_TOOLS_BASE)/* $(SPRD_OTA_TOOLS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/e2fsdroid $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/brotli $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/bsdiff $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/imgdiff $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/img2simg $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/simg2img $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/minigzip $(SPRD_OTA_PREBUILD_BINS)/
	$(hide) $(ACP) -p $(HOST_OUT_EXECUTABLES)/mkbootfs $(SPRD_OTA_PREBUILD_BINS)/

	$(hide) $(ACP) -p $(HOST_OUT_JAVA_LIBRARIES)/signapk.jar $(SPRD_OTA_PREBUILD_SIGNAPK)/

	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libc++.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libbrotli.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libconscrypt_openjdk_jni.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libbase.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libcrypto-host.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libcrypto_utils.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libext4_utils.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/liblog.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/liblp.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libsparse-host.so $(SPRD_OTA_PREBUILD_LIB64)/
	$(hide) $(ACP) -p $(HOST_LIBRARY_PATH)/libz-host.so $(SPRD_OTA_PREBUILD_LIB64)/


	$(hide) mkdir -p $(SPRD_OTA_BINS)
	$(hide) $(ACP) -p $(OTA_BINS) $(SPRD_OTA_BINS)/

	$(hide) (cd $(SPRD_OTA_TOOLS) && zip -qry ../otatools.zip .)

.PHONY: sprdbuildotatool
sprdbuildotatool: $(SPRD_OTA_TOOLS)
otatools: $(SPRD_OTA_TOOLS)

