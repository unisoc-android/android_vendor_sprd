#!/bin/bash
#
# Author: bill.ji@spreadtrum.com
#
# secureboot avb2.0: sign modem bins
#
# Need environment
# bash
# awk
#
# Command usage:
#${BUILD_DIR}/avb_sign_modem.sh  ${IMG_DIR} ${PROJ} ${PAC_CONFILE} ${MODEM_DFS} ${MODEM_MODEM_W} ${MODEM_DSP_W_L} ${MODEM_DSP_W_G}
#
# **************** #
# input parameters #
# **************** #
#IMG_DIR=$1
IMG_DIR=$PWD
echo "IMG_DIR :${IMG_DIR}"
MODEM_DIR=$1
echo "MODEM_DIR :${MODEM_DIR}"
PROJ=$2
echo "PROJ:${PROJ}"
PAC_CONFILE=${OUT}/*.xml
echo "PAC_CONFILE:${PAC_CONFILE}"

DFS=$MODEM_DIR/pmsys.bin
echo "DFS:${DFS}"
MODEM_LTE=$MODEM_DIR/ltemodem.bin
echo "MODEM_LTE:${MODEM_LTE}"
DSP_LTE_L=$MODEM_DIR/ltedsp.bin
DSP_LTE_TG=$MODEM_DIR/ltegdsp.bin
DSP_LTE_AG=$MODEM_DIR/lteagdsp.bin
DSP_LTE_C=$MODEM_DIR/ltecdsp.bin

# For isharkl2
DSP_W_G=$MODEM_DIR/ltetgdsp.bin # l_tgdsp

# For WCDMA
MODEM_WCDMA=$MODEM_DIR/wmodem.bin
DSP_WCDMA_G=$MODEM_DIR/wgdsp.bin

# *************** #
# var declaration #
# *************** #
MB=1048576
AVB_TOOL=$IMG_DIR/out/host/linux-x86/bin/avbtool
echo "AVB_TOOL:${AVB_TOOL}"
SPLIT_TOOL=$IMG_DIR/out/host/linux-x86/bin/splitimg
echo "SPLIT_TOOL:${SPLIT_TOOL}"
MODEM_KEY=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sprd/config/rsa4096_modem.pem
echo "MODEM_KEY:${MODEM_KEY}"
VER_CONFIG=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sprd/config/version.cfg
echo "VER_CONFIG:${VER_CONFIG}"
SPRD_SECURE_FILE=${OUT}/PRODUCT_SECURE_BOOT_SPRD
echo "SPRD_SECURE_FILE:${SPRD_SECURE_FILE}"
DAT_VBMETA=${OUT}/dat_vbmeta.img
echo "DAT_VBMETA:${DAT_VBMETA}"
# ********* #
# functions #
# ********* #
getImageName()
{
    local imagename=$1
    echo ${imagename##*/}
}

doInit()
{
    if [ ! -f "$AVB_TOOL" ]; then
        echo "avbtool not found! exit"
        exit
    fi

    if [ ! -f "$SPLIT_TOOL" ]; then
        echo "splitimg not found! exit"
        exit
    fi

    if [ $# -eq 7 ]; then
        echo "shark series"
        CPBASE=shark
        DFS=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DFS)
        MODEM_LTE=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_MODEM_W)
        DSP_LTE_LDSP=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DSP_W_L)
        DSP_LTE_TG=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DSP_W_G)
    elif [ $# -eq 8 ]; then
        echo "whale series"
        CPBASE=whale
        DFS=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DFS)
        MODEM_LTE=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_MODEM_W)
        DSP_LTE_LDSP=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DSP_W_L)
        DSP_LTE_TG=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DSP_W_G)
        DSP_LTE_AG=${IMG_DIR}/${PROJ}/$(getImageName $MODEM_DSP_AG)
    else
        echo "para number = $#"
        echo "Input parameter error"
        exit
    fi
}

doModemCopy()
{
    echo "enter doModemCopy"
    case $CPBASE in
    shark)
        echo ${IMG_DIR}/${PROJ}
        cp $MODEM_DFS ${IMG_DIR}/${PROJ}/
        echo $MODEM_DFS
        cp $MODEM_MODEM_W ${IMG_DIR}/${PROJ}/
        echo $MODEM_MODEM_W
        cp $MODEM_DSP_W_L ${IMG_DIR}/${PROJ}/
        echo $MODEM_DSP_W_L
        cp $MODEM_DSP_W_G ${IMG_DIR}/${PROJ}/
        echo $MODEM_DSP_W_G;;
    whale)
        echo ${IMG_DIR}/${PROJ}
        cp $MODEM_DFS ${IMG_DIR}/${PROJ}/
        echo $MODEM_DFS
        cp $MODEM_MODEM_W ${IMG_DIR}/${PROJ}/
        echo $MODEM_MODEM_W
        cp $MODEM_DSP_W_L ${IMG_DIR}/${PROJ}/
        echo $MODEM_DSP_W_L
        cp $MODEM_DSP_W_G ${IMG_DIR}/${PROJ}/
        echo $MODEM_DSP_W_G
        cp $MODEM_DSP_AG ${IMG_DIR}/${PROJ}/
        echo $MODEM_DSP_AG;;
    *)
        echo "Input parameter error"
        exit;;
    esac
    echo "ls img_dir/proj"
    ls ${IMG_DIR}/${PROJ}
    echo "leave doModemCopy "
}

getImageSize()
{
    cp_arr=(pm_sys l_modem l_ldsp l_gdsp l_agdsp l_tgdsp w_modem w_gdsp l_cdsp)
    cpname_arr=($DFS $MODEM_LTE $DSP_LTE_L $DSP_LTE_TG $DSP_LTE_AG $DSP_W_G $MODEM_WCDMA $DSP_WCDMA_G $DSP_LTE_C)

    echo "enter getImageSize"
    length=${#cpname_arr[@]}
    for (( i=0; i<=$length; i++ ))
    do
        value=${cpname_arr[i]}
        if [ $value = $1 ]; then
            echo "${cpname_arr[i]} matched: ${cp_arr[i]}"
            size=`awk '/Partition id="'${cp_arr[i]}'"/{print $3}' $PAC_CONFILE | cut -d\" -f2`
            echo "size = $size"
            cur_partion=${cp_arr[i]}
            break;
        fi
    done
#    echo "leave getImageSize"
}

getRollbackIndex()
{
    rollback_index=0
    echo "enter getRollbackIndex"
    if [ ! -f "$VER_CONFIG" ]; then
        echo "version config file not found!"
    else
       rollback_index=`sed -n '/avb_version_modem/p'  $VER_CONFIG | sed -n 's/avb_version_modem=//gp'`
    fi
    echo "rollback_index = $rollback_index"
}

doAvbSignImage()
{
    echo "enter doAvbSignImage"
    # get partion size
    for loop in $@
    do
        getImageSize $loop
        if [ $size -gt 0 ]; then
            sizeInByte=$(expr $size \* $MB)
        else
            echo "size($size) is wrong"
            sizeInByte=0
            continue
        fi
        echo "sizeInByte = $sizeInByte"
        # start sign
        echo "check dat cp"
        $SPLIT_TOOL $loop
        echo "start sign"
        divname=$loop.div
        if [ -f "$divname" ]; then
            echo "found dat cp, will joint sign"
            $AVB_TOOL add_hash_footer --image $divname \
                                      --partition_size $sizeInByte \
                                      --partition_name $cur_partion \
                                      --algorithm SHA256_RSA4096 \
                                      --key $MODEM_KEY \
                                      --rollback_index $rollback_index \
                                      --output_vbmeta_image $DAT_VBMETA \
                                      --do_not_append_vbmeta_image
            if [ $? -ne 0 ]; then
                echo "Sign $loop failed!"
                rm $loop
                rm $divname
                continue
            fi
            echo "found div file, append vbmeta"
            $AVB_TOOL append_vbmeta_image --image $loop \
                                          --partition_size $sizeInByte \
                                          --vbmeta_image $DAT_VBMETA
            if [ $? -ne 0 ]; then
                echo "Sign $loop failed!"
                rm $loop
                rm $divname
                continue
            else
                echo "Sign $loop succeed"
            fi
            rm $divname
        else
            echo "normal bin, direct sign"
            $AVB_TOOL add_hash_footer --image $loop \
                                      --partition_size $sizeInByte \
                                      --partition_name $cur_partion \
                                      --algorithm SHA256_RSA4096 \
                                      --key $MODEM_KEY \
                                      --rollback_index $rollback_index
            if [ $? -ne 0 ]; then
                echo "Sign $loop failed!"
                rm $loop
                continue
            else
                echo "Sign $loop succeed"
            fi
        fi
    done
}

doModemSignImage()
{
    needsign=0

    echo "doModemSignImage  proj = $PROJ"
    echo "IMG_DIR = $IMG_DIR"
    if (echo $PROJ | grep "spsec") 1>/dev/null 2>&1 ; then
        echo "found spsec"
        needsign=1
    elif [ -f "$SPRD_SECURE_FILE" ]; then
        echo "found sprd_secure_file"
        needsign=1
    fi
    if [ $needsign = 1 ]; then
        getRollbackIndex
        doAvbSignImage $@
    else
        echo "not secureboot proj, no need to sign!"
    fi
}

doclean()
{
    if [ -f "$DAT_VBMETA" ]; then
        rm $DAT_VBMETA
    fi
}

doSignModemImage()
{
#    doModemCopy
    doModemSignImage $@
    doclean
}

# ************* #
# main function #
# ************* #
echo "Sign modem script, ver 0.20 (9850/9832e)"
#doInit $@
doSignModemImage $DFS $MODEM_LTE $DSP_LTE_L $DSP_LTE_TG $DSP_LTE_AG $DSP_W_G $MODEM_WCDMA $DSP_WCDMA_G $DSP_LTE_C

#test command
# ./avb_sign_modem.sh ~/temp/android_build/shtest 9850ka ~/temp/android_build/shtest/sp9850ka_1h10_tee.xml sharkl2_arm7.bin SC9600_sharkl2_pubcp_modem.dat sharkl2_pubcp_LTEA_DSP.bin sharkl2_pubcp_DM_DSP.bin  
