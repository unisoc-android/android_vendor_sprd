#!/usr/bin/env python
#add by spreadtrum

import sys
if sys.hexversion < 0x02070000:
  print >> sys.stderr, "Python 2.7 or newer is required."
  sys.exit(1)
import os
import imp
import tempfile
from hashlib import sha1 as sha1

sys.path.append('./build/tools/releasetools')
import common
import sign_target_files_secureboot as sign

OPTIONS = common.OPTIONS
OPTIONS.secure_boot = False
OPTIONS.secure_boot_tool = None
OPTIONS.single_key = True

def LoadInfoDict(input_dir):
  d = {}
  try:
    with open(os.path.join(input_dir, "sprd_misc_info.txt"), "rb") as f:
      data_sprd_misc = f.read()
      d = common.LoadDictionaryFromLines(data_sprd_misc.split("\n"))
  except IOError, e:
    print "can't find sprd_misc_info.txt!"
  return d

def GetBootableImage(prebuilt_name, name, input_dir):
  prebuilt_path = os.path.join(input_dir, prebuilt_name)
  if os.path.exists(prebuilt_path):
    print "GenRecoveryPatch: find %s..." % (prebuilt_name)
    f = open(prebuilt_path, "rb")
    data = f.read()
    f.close()
    return common.File(prebuilt_name, data)
  return None

def main(argv):
  args = common.ParseOptions(argv, __doc__)
  input_dir, output_dir = args
  print "*********************************************************************"
  print "*********************************************************************"
  print "*********************************************************************"
  print os.path.join(input_dir, "recovery/root/etc/recovery.fstab")
  print "*********************************************************************"
  print "*********************************************************************"
  print "*********************************************************************"

  def read_helper(fn):
    try:
      with open(fn) as f:
        return f.read()
    except IOError as e:
      raise KeyError(fn)

  info_dict = {}
  fstab_path = os.path.join(input_dir, "recovery/root/system/etc/recovery.fstab")
  recovery_fstab_path = os.path.abspath(".") + "/" + fstab_path
  print("GenRecoveryPatch: fstab path:\n%s"%(recovery_fstab_path))
  if os.path.exists(recovery_fstab_path):
    info_dict["fstab"] = common.LoadRecoveryFSTab(read_helper, 2, recovery_fstab_path)

  recovery_img = GetBootableImage("IMAGES/recovery.img", "recovery.img", input_dir)
  boot_img = GetBootableImage("IMAGES/boot.img", "boot.img", input_dir)

  if not recovery_img or not boot_img:
    print("GenRecoveryPatch: bootimg and recoveryimg not found!")
    sys.exit(0)

  def output_sink(fn, data):
    if fn == "etc/install-recovery.sh":
      fn = "bin/install-recovery.sh"
    if data:
      with open(os.path.join(output_dir, "system", *fn.split("/")), "wb") as f:
        f.write(data)

  print("GenRecoveryPatch: Begin!")
  common.MakeRecoveryPatch(input_dir, output_sink, recovery_img, boot_img, info_dict)

if __name__ == '__main__':
  main(sys.argv[1:])
