#!/bin/bash
IMG_DIR=$PWD
MODEM_DIR=$1
echo "MODEM_DIR :${MODEM_DIR}"
PROJ=$2
echo "PROJ:${PROJ}"
MODEM_LTE=ltemodem.bin
#ltemodem.bin
DSP_LTE_TG=ltetgdsp.bin
DSP_LTE_LDSP=ltedsp.bin
DSP_LTE_GDSP=ltegdsp.bin
DSP_LTE_AG=lteagdsp.bin
#pmsys.bin
DFS=pmsys.bin

# For WCDMA
MODEM_WCDMA=$MODEM_DIR/wmodem.bin
DSP_WCDMA_G=$MODEM_DIR/wgdsp.bin

STOOL_PATH=$IMG_DIR/out/host/linux-x86/bin
SPRD_CONFIG_FILE=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sprd/config
SANSA_CONFIG_PATH=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sansa/config
SANSA_OUTPUT_PATH=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sansa/output
echo "SANSA_OUTPUT_PATH:${SANSA_OUTPUT_PATH}"
SANSA_PYPATH=$IMG_DIR/vendor/sprd/proprietories-source/packimage_scripts/signimage/sansa/python
SPRD_SECURE_FILE=$PRODUCT_OUT/PRODUCT_SECURE_BOOT_SPRD

getImageName()
{
    local imagename=$1
    echo ${imagename##*/}
}

getSignImageName()
{
    local temp1=$(getImageName $1)
    local left=${temp1%.*}
    local temp2=$(getImageName $1)
    local right=${temp2##*.}
    local signname=${left}"-sign."${right}
    echo $signname
}

getRawSignImageName()
{
    local temp1=$1
    local left=${temp1%.*}
    local temp2=$1
    local right=${temp2##*.}
    local signname=${left}"-sign."${right}
    echo $signname
}

getLogName()
{
    local temp=$(getImageName $1)
    local left=${temp%.*}
    local name=$SANSA_OUTPUT_PATH/${left}".log"
    echo $name
}

doModemInsertHeader()
{
    echo "doModemInsertHeader enter"
    for loop in $@
    do
        if [ -f $loop ] ; then
            $STOOL_PATH/imgheaderinsert $loop 0
        else
            echo "#### no $loop,please check ####"
        fi
    done

    echo "doModemInsertHeader leave. now list"
	  ls ${MODEM_DIR}
    echo "list end"
}

makeModemCntCert()
{
    echo
    echo "makeModemCntCert: $1 "

    #local CNTCFG=${SANSA_CONFIG_PATH}/certcnt_modem.cfg
    local CNTCFG=${SANSA_CONFIG_PATH}/certcnt_3.cfg
    local SW_TBL=${SANSA_CONFIG_PATH}/SW.tbl

    sed -i "s#.* .* .*#$1 0xFFFFFFFFFFFFFFFF 0x200#" $SW_TBL
    if [ -f $1 ] ; then
        #cd $IMG_DIR/..
        #echo "switch to: `pwd`"
        echo "sign......"
        python3 $SANSA_PYPATH/bin/cert_sb_content_util.py $CNTCFG $(getLogName $1)
        #echo "back to:"
        #cd -
        #echo
    else
        echo "#### no $1,please check ####"
    fi
}

genModemCntCfgFile()
{
    local CNTCFG=${SANSA_CONFIG_PATH}/certcnt_modem.cfg
    local KEY=${SANSA_CONFIG_PATH}/key_3.pem
    local KEYPWD=${SANSA_CONFIG_PATH}/pass_3.txt
    local SWTBL=${SANSA_CONFIG_PATH}/SW.tbl
    local NVID=2
    local NVVAL=1
    local CERTPKG=${SANSA_OUTPUT_PATH}/certcnt.bin

    echo "enter genModemCntCfgFile "

    if [ -f "$CNTCFG" ]; then
        rm -rf $CNTCFG
    fi

    touch ${CNTCFG}
    echo "[CNT-CFG]" >> ${CNTCFG}
    echo "cert-keypair = ${KEY}" >> ${CNTCFG}
    echo "cert-keypair-pwd = ${KEYPWD}" >> ${CNTCFG}
    echo "images-table = ${SWTBL}" >> ${CNTCFG}
    echo "nvcounter-id = ${NVID}" >> ${CNTCFG}
    echo "nvcounter-val = ${NVVAL}" >> ${CNTCFG}
    echo "cert-pkg = ${CERTPKG}" >> ${CNTCFG}
}

doSansaModemSignImage()
{
    echo "doSansaModemSignImage: $@ "
    #genModemCntCfgFile

    for loop in $@
    do
        if [ -f $loop ] ; then
            echo "call splitimg"
            $STOOL_PATH/splitimg $loop
            makeModemCntCert $loop
            $STOOL_PATH/signimage $SANSA_OUTPUT_PATH $(getRawSignImageName $loop) 1 0
        else
            echo "#### no $loop,please check ####"
        fi
    done
    echo "doSansaModemSignImage leave. now list"
	ls ${MODEM_DIR}
    echo "list end"
}

doSprdModemSignImage()
{
    for loop in $@
    do
        if [ -f $loop ] ; then
	    $STOOL_PATH/sprd_sign $(getRawSignImageName $loop) $SPRD_CONFIG_FILE
        else
            echo "#### no $loop,please check ####"
        fi
    done
}

doModemSignImage()
{
    echo "doModemSignImage  proj = $PROJ"
	if (echo $PROJ | grep "spsec") 1>/dev/null 2>&1 ; then
        doSprdModemSignImage $@
	elif [ -f "$SPRD_SECURE_FILE" ]; then
		echo "found sprd_secure_file"
		doSprdModemSignImage $@
    else
        if (echo $PROJ | grep "dxsec") 1>/dev/null 2>&1 ; then
            doSansaModemSignImage $@
        fi
    fi
}

doModemPackImage()
{
	ptime
    doModemInsertHeader $@
	ptime
    doModemSignImage $@
	ptime
}

function ptime()
{
    local start_time=$(date +"%s")
    local tdiff=$start_time
    local hours=$(($tdiff / 3600 ))
    local mins=$((($tdiff % 3600) / 60))
    local secs=$(($tdiff % 60))
    echo

    if [ $hours -gt 0 ] ; then
        printf "###(%02g:%02g:%02g (hh:mm:ss)) ###" $hours $mins $secs
    elif [ $mins -gt 0 ] ; then
        printf "(%02g:%02g (mm:ss))" $mins $secs
    elif [ $secs -gt 0 ] ; then
        printf "(%s seconds)" $secs
    fi
    echo
}


doModemPackImage $MODEM_DIR/$MODEM_LTE $MODEM_DIR/$DSP_LTE_TG $MODEM_DIR/$DSP_LTE_LDSP $MODEM_DIR/$DSP_LTE_GDSP $MODEM_DIR/$DSP_LTE_AG $MODEM_DIR/$DFS $MODEM_DIR/$MODEM_WCDMA $DSP_WCDMA_G

echo "Delete un-signed bins and rename the bins:"
echo "delete $MODEM_DIR/$MODEM_LTE "
echo "mv $MODEM_DIR/$(getSignImageName $MODEM_LTE) to  $MODEM_DIR/$MODEM_LTE "
rm -f $MODEM_DIR/$MODEM_LTE
mv $MODEM_DIR/$(getSignImageName $MODEM_LTE) $MODEM_DIR/$MODEM_LTE
rm -f $MODEM_DIR/$DSP_LTE_TG
mv $MODEM_DIR/$(getSignImageName $DSP_LTE_TG) $MODEM_DIR/$DSP_LTE_TG
rm -f $MODEM_DIR/$DSP_LTE_LDSP
mv $MODEM_DIR/$(getSignImageName $DSP_LTE_LDSP) $MODEM_DIR/$DSP_LTE_LDSP
rm -f $MODEM_DIR/$DSP_LTE_GDSP
mv $MODEM_DIR/$(getSignImageName $DSP_LTE_GDSP) $MODEM_DIR/$DSP_LTE_GDSP
rm -f $MODEM_DIR/$DSP_LTE_AG
mv $MODEM_DIR/$(getSignImageName $DSP_LTE_AG) $MODEM_DIR/$DSP_LTE_AG
rm -f $MODEM_DIR/$DFS
mv $MODEM_DIR/$(getSignImageName $DFS) $MODEM_DIR/$DFS
# For WCDMA
rm -f $MODEM_DIR/$MODEM_WCDMA
mv $MODEM_DIR/$(getSignImageName $MODEM_WCDMA) $MODEM_DIR/$MODEM_WCDMA
rm -f $MODEM_DIR/$DSP_WCDMA_G
mv $MODEM_DIR/$(getSignImageName $DSP_WCDMA_G) $MODEM_DIR/$DSP_WCDMA_G
echo "after rename signed images list:"
ls ${MODEM_DIR}
echo "list end"
