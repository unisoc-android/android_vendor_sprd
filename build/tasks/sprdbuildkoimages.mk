#
## For building unisoc ko partitions
#

# Creates ko partiton symlinks in /system
# $(1): lowercase partition name
define create-system-koimage-symlink
if [ -d $(TARGET_OUT)/$(1) ] && [ ! -h $(TARGET_OUT)/$(1) ]; then \
  echo 'Non-symlink $(TARGET_OUT)/$(1) detected!' 1>&2; \
  echo 'You cannot install files to $(TARGET_OUT)/$(1) while building a separate $(1).img!' 1>&2; \
  exit 1; \
fi
ln -sf /mnt/vendor/$(1) $(TARGET_OUT)/$(1)
endef

# Generates prop dict for ko image
# $(1): prop dict file path
# $(2): socko/odmko
define generate-prop-dictionary-for-koimage
$(if $(filter $(2),socko),
  $(if $(BOARD_SOCKOIMAGE_FILE_SYSTEM_TYPE),echo "socko_fs_type=$(BOARD_SOCKOIMAGE_FILE_SYSTEM_TYPE)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_EXTFS_INODE_COUNT),echo "socko_extfs_inode_count=$(BOARD_SOCKOIMAGE_EXTFS_INODE_COUNT)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_EXTFS_RSV_PCT),echo "socko_extfs_rsv_pct=$(BOARD_SOCKOIMAGE_EXTFS_RSV_PCT)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_PARTITION_SIZE),echo "socko_size=$(BOARD_SOCKOIMAGE_PARTITION_SIZE)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_JOURNAL_SIZE),echo "socko_journal_size=$(BOARD_SOCKOIMAGE_JOURNAL_SIZE)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_SQUASHFS_COMPRESSOR),echo "socko_squashfs_compressor=$(BOARD_SOCKOIMAGE_SQUASHFS_COMPRESSOR)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_SQUASHFS_COMPRESSOR_OPT),echo "socko_squashfs_compressor_opt=$(BOARD_SOCKOIMAGE_SQUASHFS_COMPRESSOR_OPT)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_SQUASHFS_BLOCK_SIZE),echo "socko_squashfs_block_size=$(BOARD_SOCKOIMAGE_SQUASHFS_BLOCK_SIZE)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_SQUASHFS_DISABLE_4K_ALIGN),echo "socko_squashfs_disable_4k_align=$(BOARD_SOCKOIMAGE_SQUASHFS_DISABLE_4K_ALIGN)" >> $(1))
  $(if $(PRODUCT_SOCKO_BASE_FS_PATH),echo "socko_base_fs_file=$(PRODUCT_SOCKO_BASE_FS_PATH)" >> $(1))
  $(if $(BOARD_SOCKOIMAGE_PARTITION_RESERVED_SIZE),echo "socko_reserved_size=$(BOARD_SOCKOIMAGE_PARTITION_RESERVED_SIZE)" >> $(1))
  $(if $(BOARD_AVB_ENABLE),
    echo "avb_socko_hashtree_enable=$(BOARD_AVB_ENABLE)" >> $(1)
    echo "avb_socko_add_hashtree_footer_args=$(BOARD_AVB_SOCKO_ADD_HASHTREE_FOOTER_ARGS)" >> $(1)
    $(if $(BOARD_AVB_SOCKO_KEY_PATH),
      echo "avb_socko_key_path=$(BOARD_AVB_SOCKO_KEY_PATH)" >> $(1)
      echo "avb_socko_algorithm=$(BOARD_AVB_SOCKO_ALGORITHM)" >> $(1)
      echo "avb_socko_rollback_index_location=$(BOARD_AVB_SOCKO_ROLLBACK_INDEX_LOCATION)" >> $(1)
	)
    $(if $(PRODUCT_SOCKO_VERITY_PARTITION),echo "socko_verity_block_device=$(PRODUCT_SOCKO_VERITY_PARTITION)" >> $(1))
  )
)
$(if $(filter $(2),odmko),
  $(if $(BOARD_ODMKOIMAGE_FILE_SYSTEM_TYPE),echo "odmko_fs_type=$(BOARD_ODMKOIMAGE_FILE_SYSTEM_TYPE)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_EXTFS_INODE_COUNT),echo "odmko_extfs_inode_count=$(BOARD_ODMKOIMAGE_EXTFS_INODE_COUNT)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_EXTFS_RSV_PCT),echo "odmko_extfs_rsv_pct=$(BOARD_ODMKOIMAGE_EXTFS_RSV_PCT)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_PARTITION_SIZE),echo "odmko_size=$(BOARD_ODMKOIMAGE_PARTITION_SIZE)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_JOURNAL_SIZE),echo "odmko_journal_size=$(BOARD_ODMKOIMAGE_JOURNAL_SIZE)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_SQUASHFS_COMPRESSOR),echo "odmko_squashfs_compressor=$(BOARD_ODMKOIMAGE_SQUASHFS_COMPRESSOR)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_SQUASHFS_COMPRESSOR_OPT),echo "odmko_squashfs_compressor_opt=$(BOARD_ODMKOIMAGE_SQUASHFS_COMPRESSOR_OPT)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_SQUASHFS_BLOCK_SIZE),echo "odmko_squashfs_block_size=$(BOARD_ODMKOIMAGE_SQUASHFS_BLOCK_SIZE)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_SQUASHFS_DISABLE_4K_ALIGN),echo "odmko_squashfs_disable_4k_align=$(BOARD_ODMKOIMAGE_SQUASHFS_DISABLE_4K_ALIGN)" >> $(1))
  $(if $(PRODUCT_ODMKO_BASE_FS_PATH),echo "odmko_base_fs_file=$(PRODUCT_ODMKO_BASE_FS_PATH)" >> $(1))
  $(if $(BOARD_ODMKOIMAGE_PARTITION_RESERVED_SIZE),echo "odmko_reserved_size=$(BOARD_ODMKOIMAGE_PARTITION_RESERVED_SIZE)" >> $(1))
  $(if $(BOARD_AVB_ENABLE),
    echo "avb_odmko_hashtree_enable=$(BOARD_AVB_ENABLE)" >> $(1)
    echo "avb_odmko_add_hashtree_footer_args=$(BOARD_AVB_ODMKO_ADD_HASHTREE_FOOTER_ARGS)" >> $(1)
    $(if $(BOARD_AVB_ODMKO_KEY_PATH),
      echo "avb_odmko_key_path=$(BOARD_AVB_ODMKO_KEY_PATH)" >> $(1)
      echo "avb_odmko_algorithm=$(BOARD_AVB_ODMKO_ALGORITHM)" >> $(1)
      echo "avb_odmko_rollback_index_location=$(BOARD_AVB_ODMKO_ROLLBACK_INDEX_LOCATION)" >> $(1)
	)
    $(if $(PRODUCT_ODMKO_VERITY_PARTITION),echo "odmko_verity_block_device=$(PRODUCT_ODMKO_VERITY_PARTITION)" >> $(1))
  )
)
endef

# Generates ko list for specific ko partition
# Basic variables should be:
# xxx_KO_DIRS, xxx_KO_NAMES, xxx_KO_LIST
# $(1): partition name
define generate_ko_list_for_koimage
$(eval pname_lower := $(call to-lower,$(strip $(1)))) \
$(eval pname_upper := $(call to-upper,$(strip $(1)))) \
$(info Generating ko list for $(pname_lower) ...) \
$(foreach name,$(PRODUCT_$(pname_upper)_KO_NAMES),\
	$(eval ko_path := $(shell find $(PRODUCT_$(pname_upper)_KO_DIRS) -type f -name "$(name).ko")) \
	$(if $(ko_path),$(eval PRODUCT_$(pname_upper)_KO_LIST += $(ko_path)) $(eval ko_path :=),\
	$(error Prebuilt $(name).ko needed for building $(pname_lower) image))) \
$(eval pname_lower :=) \
$(eval pname_upper :=)
endef

# Gets the specific ko image
# $(1): partition name
define build-koimage-target
$(eval pname_lower := $(call to-lower,$(strip $(1))))
$(eval pname_upper := $(call to-upper,$(strip $(1))))
$(call pretty,"Target $(pname_lower) fs image: $(INSTALLED_$(pname_upper)IMAGE_TARGET)")
$(call pretty,"PRODUCT_$(pname_upper)_KO_LIST: $(PRODUCT_$(pname_upper)_KO_LIST)")
$(eval $(pname_lower)_image_info_file := $(call intermediates-dir-for,PACKAGING,$(pname_lower))/$(pname_lower)_image_info.txt)
mkdir -p $(dir $($(pname_lower)_image_info_file)) && rm -rf $($(pname_lower)_image_info_file)
$(call generate-image-prop-dictionary,$($(pname_lower)_image_info_file),$(pname_lower),skip_fsck=true)
rm -rf $(TARGET_OUT_$(pname_upper)) && mkdir -p $(TARGET_OUT_$(pname_upper))
$(if $(strip $(PRODUCT_$(pname_upper)_KO_LIST)),cp $(PRODUCT_$(pname_upper)_KO_LIST) $(TARGET_OUT_$(pname_upper)))
PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH \
	build/make/tools/releasetools/build_image.py \
	$(TARGET_OUT_$(pname_upper)) $($(pname_lower)_image_info_file) $(INSTALLED_$(pname_upper)IMAGE_TARGET) $(TARGET_OUT)
$(call assert-max-image-size,$(INSTALLED_$(pname_upper)IMAGE_TARGET),$(BOARD_$(pname_upper)IMAGE_PARTITION_SIZE))
$(eval pname_lower :=)
$(eval pname_upper :=)
endef

# socko partition image
ifdef BUILDING_SOCKO_IMAGE

# Gets ko list by ko dirs and ko names
ifeq ($(strip $(PRODUCT_SOCKO_KO_LIST)),)
$(call generate_ko_list_for_koimage,socko)
endif

INSTALLED_SOCKOIMAGE_TARGET := $(PRODUCT_OUT)/socko.img
INSTALLED_SOCKOIMAGE_TARGET_DEPS := \
    $(INTERNAL_USERIMAGES_DEPS) \
    $(BUILD_IMAGE_SRCS) \
	$(PRODUCT_SOCKO_KO_LIST)

$(INSTALLED_SOCKOIMAGE_TARGET): $(INSTALLED_SOCKOIMAGE_TARGET_DEPS)
	$(hide) $(call build-koimage-target,socko)

$(BUILT_TARGET_FILES_PACKAGE): $(INSTALLED_SOCKOIMAGE_TARGET)
droidcore: $(INSTALLED_SOCKOIMAGE_TARGET)

.PHONY: sockoimage
sockoimage: $(INSTALLED_SOCKOIMAGE_TARGET)

endif # BUILDING_SOCKO_IMAGE

ifdef BUILDING_ODMKO_IMAGE

# Gets ko list by ko dirs and ko names
ifeq ($(strip $(PRODUCT_ODMKO_KO_LIST)),)
$(call generate_ko_list_for_koimage,odmko)
endif

INSTALLED_ODMKOIMAGE_TARGET := $(PRODUCT_OUT)/odmko.img
INSTALLED_ODMKOIMAGE_TARGET_DEPS := \
    $(INTERNAL_USERIMAGES_DEPS) \
    $(BUILD_IMAGE_SRCS) \
	$(PRODUCT_ODMKO_KO_LIST)

$(INSTALLED_ODMKOIMAGE_TARGET): $(INSTALLED_ODMKOIMAGE_TARGET_DEPS)
	$(hide) $(call build-koimage-target,odmko)

$(BUILT_TARGET_FILES_PACKAGE): $(INSTALLED_ODMKOIMAGE_TARGET)
droidcore: $(INSTALLED_ODMKOIMAGE_TARGET)

.PHONY: odmkoimage
odmkoimage: $(INSTALLED_ODMKOIMAGE_TARGET)

endif
