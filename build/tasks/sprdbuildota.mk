LOCAL_PATH := $(call my-dir)

LOCAL_NVMERGE_INTERMEDIATES_FILE := $(call intermediates-dir-for,EXECUTABLES,nvmerge,,,$(TARGET_PREFER_32_BIT))/nvmerge
#LOCAL_REPART_INTERMEDIATES_FILE := $(call intermediates-dir-for,EXECUTABLES,repart,,,$(TARGET_PREFER_32_BIT))/repart
LOCAL_MKF2FS_INTERMEDIATES_FILE := $(TARGET_RECOVERY_ROOT_OUT)/system/bin/make_f2fs
LOCAL_SLOADF2FS_INTERMEDIATES_FILE := $(TARGET_RECOVERY_ROOT_OUT)/system/bin/sload_f2fs


MODEM_UPDATING_TOOLS := $(LOCAL_NVMERGE_INTERMEDIATES_FILE) \
	$(TARGET_RECOVERY_NVMERGE_CONFIG) \
	$(MODEM_UPDATE_CONFIG_FILE) \
	$(OTA_PTOQ_REPART_BIN) \
	$(LOCAL_MKF2FS_INTERMEDIATES_FILE) \
	$(LOCAL_SLOADF2FS_INTERMEDIATES_FILE)

$(BUILT_TARGET_FILES_PACKAGE) : $(MODEM_UPDATING_TOOLS)
$(BUILT_TARGET_FILES_PACKAGE) : PRIVATE_OTA_TOOLS += $(MODEM_UPDATING_TOOLS)

#-------------------------------------------------------------------------------------------------

SPRD_BUILT_TARGET_FILES_PACKAGE := $(basename $(BUILT_TARGET_FILES_PACKAGE))

$(SPRD_BUILT_TARGET_FILES_PACKAGE): $(BUILT_TARGET_FILES_PACKAGE)
	$(hide) mkdir -p $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES
	$(hide) $(ACP) $(PRODUCT_OUT)/boot.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/boot.img
	$(hide) $(ACP) $(PRODUCT_OUT)/recovery.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/recovery.img
	$(hide) -$(ACP) $(PRODUCT_OUT)/vbmeta-sign.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/vbmeta.img

	#$(hide) $(ACP) $(PRODUCT_OUT)/u-boot.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/u-boot.bin
	#$(hide) $(ACP) $(PRODUCT_OUT)/u-boot-spl-16k.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/u-boot-spl-16k.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/u-boot-sign.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/u-boot.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/u-boot-spl-16k-sign.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/u-boot-spl-16k.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/tos-sign.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/tos.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/sml-sign.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/sml.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/teecfg-sign.bin $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/teecfg.bin
	$(hide) -$(ACP) $(PRODUCT_OUT)/dtbo.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/dtbo.img
	$(hide) -$(ACP) $(BOARDDIR)/$(TARGET_BOARD).xml $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/partition.xml

	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH MKBOOTIMG=$(MKBOOTIMG) \
	    ./build/tools/releasetools/make_recovery_patch -v $(SPRD_BUILT_TARGET_FILES_PACKAGE) $(SPRD_BUILT_TARGET_FILES_PACKAGE)

	$(hide) echo "PRODUCT_SECURE_BOOT=$(PRODUCT_SECURE_BOOT)"
ifneq (,$(filter $(PRODUCT_SECURE_BOOT),SANSA SPRD))
	$(hide) echo "secure_boot=$(PRODUCT_SECURE_BOOT)" >> $(SPRD_BUILT_TARGET_FILES_PACKAGE)/META/misc_info.txt
	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH \
	    ./vendor/sprd/build/tasks/otascripts/avb_sign_modem.sh $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO $(TARGET_PRODUCT) \
	    rm -rf $<; \
	    sleep 30; \
	    PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH MKBOOTIMG=$(MKBOOTIMG) \
	    ./build/tools/releasetools/add_img_to_target_files -a -v -p $(HOST_OUT) $(SPRD_BUILT_TARGET_FILES_PACKAGE)

	$(hide) $(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/vbmeta_system.img $(PRODUCT_OUT)/vbmeta_system.img -rfv
	$(hide) $(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/vbmeta_vendor.img $(PRODUCT_OUT)/vbmeta_vendor.img -rfv
	$(hide) $(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/vbmeta_system.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/vbmeta_system.img -rfv
	$(hide) $(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/vbmeta_vendor.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/RADIO/vbmeta_vendor.img -rfv
else

	$(hide) rm -rf $<
	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH MKBOOTIMG=$(MKBOOTIMG) \
	    ./build/tools/releasetools/add_img_to_target_files -a -v -p $(HOST_OUT) $(SPRD_BUILT_TARGET_FILES_PACKAGE)
endif

	$(hide) $(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/socko.img $(PRODUCT_OUT)/socko.img
	$(hide) -$(ACP) $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/odmko.img $(PRODUCT_OUT)/odmko.img

	#Gen a raw odmko img to write into partition when p to q, since even it is empty, it still be veified in bootloader
	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH \
	    $(SIMG2IMG) $(PRODUCT_OUT)/odmko.img $(SPRD_BUILT_TARGET_FILES_PACKAGE)/IMAGES/odmko_raw.img

	#Gen a metadata only super and write into partition when p to q, since metadata builder will gen new data based on existing one
	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH \
	    ./vendor/sprd/build/tasks/otascripts/build_super_mo_image.py $(PRODUCT_OUT) $(SPRD_BUILT_TARGET_FILES_PACKAGE)


	# zip all target-files here incase of all signed bootloader/dtb/avb/modembins/boot&recovery are collected
	$(hide) find $(SPRD_BUILT_TARGET_FILES_PACKAGE)/META | sort >$<.list
	$(hide) find $(SPRD_BUILT_TARGET_FILES_PACKAGE) -path $(SPRD_BUILT_TARGET_FILES_PACKAGE)/META -prune -o -print | sort >>$<.list
	$(hide) $(SOONG_ZIP) -d -o $< -C $(SPRD_BUILT_TARGET_FILES_PACKAGE) -l $<.list


	# rebuild super.img because the system/vendor/product imgs are rebuild when make target-file-package again
	@echo "Rebuild super fs image after target pkg: $(INSTALLED_SUPERIMAGE_TARGET)"
	$(hide) PATH=$(dir $(LPMAKE)):$$PATH \
	    $(BUILD_SUPER_IMAGE) -v $(SPRD_BUILT_TARGET_FILES_PACKAGE) $(INSTALLED_SUPERIMAGE_TARGET)


.PHONY: sprd_built_packages
sprd_built_packages: $(SPRD_BUILT_TARGET_FILES_PACKAGE)

target-files-package: $(SPRD_BUILT_TARGET_FILES_PACKAGE)

$(INTERNAL_OTA_PACKAGE_TARGET): $(SPRD_BUILT_TARGET_FILES_PACKAGE) \
	    build/make/tools/releasetools/ota_from_target_files
	@echo "Package OTA: $@"
	$(call build-ota-package-target,$@,-k $(KEY_CERT_PAIR) --output_metadata_path $(INTERNAL_OTA_METADATA))

$(INTERNAL_OTA_RETROFIT_DYNAMIC_PARTITIONS_PACKAGE_TARGET): $(SPRD_BUILT_TARGET_FILES_PACKAGE) \
	    build/make/tools/releasetools/ota_from_target_files
	@echo "Package OTA (retrofit dynamic partitions): $@"
	$(call build-ota-package-target,$@,-k $(KEY_CERT_PAIR) --retrofit_dynamic_partitions)

ifeq (,$(filter true, $(TARGET_SIMULATOR)))
intermediates := $(call intermediates-dir-for,PACKAGING,recovery_patch_unisoc)
RECOVERY-FROM-BOOT-PATCH:= $(intermediates)/recovery-from-boot.p
$(RECOVERY-FROM-BOOT-PATCH): $(INSTALLED_BOOTIMAGE_TARGET) $(INSTALLED_RECOVERYIMAGE_TARGET) $(HOST_OUT_EXECUTABLES)/imgdiff $(HOST_OUT_EXECUTABLES)/bsdiff
	$(hide) mkdir -p $(PRODUCT_OUT)/IMAGES
	$(hide) $(ACP) $(PRODUCT_OUT)/boot.img $(PRODUCT_OUT)/IMAGES/boot.img
	$(hide) $(ACP) $(PRODUCT_OUT)/recovery.img $(PRODUCT_OUT)/IMAGES/recovery.img
	$(hide) PATH=$(foreach p,$(INTERNAL_USERIMAGES_BINARY_PATHS),$(p):)$$PATH \
		./vendor/sprd/build/tasks/otascripts/make_recovery_from_boot_patch.py $(PRODUCT_OUT) $(PRODUCT_OUT)
	$(hide) rm -rf $(PRODUCT_OUT)/IMAGES

.PHONY: recovery-from-bootp
recovery-from-bootp : $(RECOVERY-FROM-BOOT-PATCH)

$(BUILT_SYSTEMIMAGE): $(RECOVERY-FROM-BOOT-PATCH)
endif
