
define print-build-packages
$(hide) \
$(if $(DUMP_BUILDING_PACKAGES), \
    echo -e $(strip \"Module\",\"Type\",\"Path\")\
        $(foreach m,$(1), $(strip \
            \\\n\"$(m)\",\"$(ALL_MODULES.$(m).CLASS)\",\"$(ALL_MODULES.$(m).PATH)\")\
        ) > $(2) \
    ,echo "Sorry.. For optimize ninja size. "\
            "You must use make DUMP_BUILDING_PACKAGES=true to display this content" > $(2)\
)
endef

#####

stamp := $(PRODUCT_OUT)/dump-$(TARGET_PRODUCT)-$(TARGET_BUILD_VARIANT)-building-packages.csv

$(stamp) :
	@echo "Result will print info $@"
	$(call print-build-packages,  $(PRODUCTS.$(INTERNAL_PRODUCT).PRODUCT_PACKAGES), $@)

.PHONY: dump-building-packages
dump-building-packages : $(stamp)

#####

stamp := $(PRODUCT_OUT)/dump-$(TARGET_PRODUCT)-$(TARGET_BUILD_VARIANT)-all-packages.csv

$(stamp) :
	@echo "Result will print to $@"
	$(call print-build-packages, $(ALL_MODULES), $@)

.PHONY: dump-all-packages
dump-all-packages : $(stamp)

