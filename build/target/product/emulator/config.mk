
# This option is already deprecated, but re-use for unisoc emulator
TARGET_SIMULATOR := true

TARGET_BOARD_CAMERA_FUNCTION_DUMMY := true

# Dummy values, they should not take effects, but can avoid
# the very terrible BSP modules including build errors
TARGET_BSP_UAPI_PATH := external/kernel-headers/original/uapi
TARGET_BSP_KERNEL_PATH := external/kernel-headers/original/uapi

# For Tele packages (core modules)
PRODUCT_PACKAGES += \
    ims \
    radio_interactor_common \
    unisoc_ims_common

PRODUCT_BOOT_JARS += \
    radio_interactor_common \
    unisoc_ims_common

PRODUCT_ARTIFACT_PATH_REQUIREMENT_WHITELIST += \
    system/framework/radio_interactor_common.jar \
    system/framework/unisoc_ims_common.jar \
    system/priv-app/ims/ims.apk

# TODO, workarounds for embmsd
PRODUCT_ARTIFACT_PATH_REQUIREMENT_WHITELIST += \
    system/bin/embmsd \
    system/etc/init/embms.rc

PRODUCT_PACKAGES += DreamCamera2

# TODO, Open it in future
# For Tele packages (others)
# PRODUCT_PACKAGES += \
    MmsFolderView \
    messaging \
    Stk  \
    SprdDialer \
    UplmnSettings \
    urild \
    librilcore \
    libimpl-ril \
    libril-lite \
    librilutils \
    libatci \
    ModemNotifier \
    vendor.sprd.hardware.radio@1.0 \
    vendor.sprd.hardware.radio.lite@1.0 \
    libFactoryRadioTest \
    lib_remote_simlock \
    android.hardware.radio@1.2 \
    android.hardware.radio@1.3 \
    android.hardware.radio@1.4 \
    libril-private \
    ims \
    CallSettings \
    CallFireWall \
    SprdContacts \
    SprdContactsProvider \
    SprdMobileTracker \
    radio_interactor_service \
    radio_interactor_common \
    unisoc_ims_common \
    Stk1 \
    MsmsStk \
    SprdDialerGo

