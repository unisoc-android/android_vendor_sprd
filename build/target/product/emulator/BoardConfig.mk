
SPRD_EMULATOR_SEPOLICY_DIR := vendor/sprd/generic/sepolicy/emulator

BOARD_SEPOLICY_DIRS += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/common/vendor)
BOARD_PLAT_PRIVATE_SEPOLICY_DIR += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/common/private)
BOARD_PLAT_PUBLIC_SEPOLICY_DIR += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/common/public)

# For goldfish spec policies, maybe moved into device/generic/goldfish is better.
BOARD_SEPOLICY_DIRS += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/goldfish/vendor)
BOARD_PLAT_PRIVATE_SEPOLICY_DIR += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/goldfish/private)
BOARD_PLAT_PUBLIC_SEPOLICY_DIR += $(wildcard $(SPRD_EMULATOR_SEPOLICY_DIR)/goldfish/public)

