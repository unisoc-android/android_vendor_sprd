/*
 * Copyright (C) 2016-2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "NativeCamera"

#include <algorithm>
#include <chrono>
#include <mutex>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <condition_variable>
#include <inttypes.h>
#include <android/hardware/camera/device/1.0/ICameraDevice.h>
#include <android/hardware/camera/device/3.2/ICameraDevice.h>
#include <android/hardware/camera/device/3.3/ICameraDeviceSession.h>
#include <android/hardware/camera/device/3.4/ICameraDeviceSession.h>
#include <android/hardware/camera/device/3.4/ICameraDeviceCallback.h>
#include <android/hardware/camera/provider/2.4/ICameraProvider.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <binder/MemoryHeapBase.h>
#include <CameraMetadata.h>
#include <CameraParameters.h>
#include <cutils/properties.h>
#include <fmq/MessageQueue.h>
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
#include <gui/CpuConsumer.h>
#include <gui/ISurfaceComposer.h>
#include <hardware/gralloc.h>
#include <hardware/gralloc1.h>
#include "hardware/camera3.h"
#include <system/camera.h>
#include <system/camera_metadata.h>
#include <ui/GraphicBuffer.h>
#include <ui/GraphicBufferMapper.h>
#include <ui/DisplayInfo.h>
#include <android/hardware/graphics/mapper/2.0/IMapper.h>
#include <android/hardware/graphics/mapper/2.0/types.h>

using namespace ::android::hardware::camera::device;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::hardware::hidl_handle;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::sp;
using ::android::wp;
using ::android::GraphicBuffer;
using ::android::IGraphicBufferProducer;
using ::android::IGraphicBufferConsumer;
using ::android::Surface;
using ::android::hardware::graphics::common::V1_0::PixelFormat;
using ::android::hardware::camera::common::V1_0::Status;
using ::android::hardware::camera::common::V1_0::CameraDeviceStatus;
using ::android::hardware::camera::common::V1_0::TorchMode;
using ::android::hardware::camera::common::V1_0::TorchModeStatus;
using ::android::hardware::camera::common::V1_0::helper::CameraParameters;
using ::android::hardware::camera::common::V1_0::helper::Size;
using ::android::hardware::camera::provider::V2_4::ICameraProvider;
using ::android::hardware::camera::provider::V2_4::ICameraProviderCallback;
using ::android::hardware::camera::device::V3_2::ICameraDevice;
using ::android::hardware::camera::device::V3_2::BufferCache;
using ::android::hardware::camera::device::V3_2::CaptureRequest;
using ::android::hardware::camera::device::V3_2::CaptureResult;
using ::android::hardware::camera::device::V3_2::ICameraDeviceCallback;
using ::android::hardware::camera::device::V3_2::ICameraDeviceSession;
using ::android::hardware::camera::device::V3_2::NotifyMsg;
using ::android::hardware::camera::device::V3_2::RequestTemplate;
using ::android::hardware::camera::device::V3_2::StreamType;
using ::android::hardware::camera::device::V3_2::StreamRotation;
using ::android::hardware::camera::device::V3_2::StreamConfiguration;
using ::android::hardware::camera::device::V3_2::StreamConfigurationMode;
using ::android::hardware::camera::device::V3_2::CameraMetadata;
using ::android::hardware::camera::device::V3_2::HalStreamConfiguration;
using ::android::hardware::camera::device::V3_2::BufferStatus;
using ::android::hardware::camera::device::V3_2::StreamBuffer;
using ::android::hardware::camera::device::V3_2::MsgType;
using ::android::hardware::camera::device::V3_2::ErrorCode;
using ::android::hardware::camera::device::V1_0::NotifyCallbackMsg;
using ::android::hardware::camera::device::V1_0::DataCallbackMsg;
using ::android::hardware::MessageQueue;
using ::android::hardware::kSynchronizedReadWrite;

using ResultMetadataQueue = MessageQueue<uint8_t, kSynchronizedReadWrite>;
using ::android::hidl::manager::V1_0::IServiceManager;

using namespace ::android::hardware::camera;

#define V(fmt, args...)	do {ALOGV(fmt, ##args); printf(fmt "\n", ##args);}while(0)
#define D(fmt, args...)	do {ALOGD(fmt, ##args); printf(fmt "\n", ##args);}while(0)
#define E(fmt, args...)	do {ALOGE(fmt, ##args); printf(fmt "\n", ##args);}while(0)
#define UNUSED(x) (void)(x)
#ifndef container_of
#define container_of(ptr, type, member) ({                      \
        const __typeof__(((type *) 0)->member) *__mptr = (ptr); 	\
        (type *) ((char *) __mptr - (char *)(&((type *)0)->member)); })
#endif

struct AvailableStream {
    int32_t width;
    int32_t height;
    int32_t format;
};


namespace
{
// "device@<version>/legacy/<id>"
const char *kDeviceNameRE = "device@([0-9]+\\.[0-9]+)/%s/(.+)";
const int CAMERA_DEVICE_API_VERSION_3_4_1 = 0x304;
const int CAMERA_DEVICE_API_VERSION_3_3_1 = 0x303;
const int CAMERA_DEVICE_API_VERSION_3_2_1 = 0x302;
const int CAMERA_DEVICE_API_VERSION_1_0_1 = 0x100;
const char *kHAL3_4 = "3.4";
const char *kHAL3_3 = "3.3";
const char *kHAL3_2 = "3.2";
const char *kHAL1_0 = "1.0";

int g_sensor_width = 1440;
int g_sensor_height = 1080;


bool matchDeviceName(const hidl_string &deviceName,
                     const hidl_string &providerType,
                     std::string *deviceVersion,
                     std::string *cameraId)
{
    ::android::String8 pattern;
    pattern.appendFormat(kDeviceNameRE, providerType.c_str());
    std::regex e(pattern.string());
    std::string deviceNameStd(deviceName.c_str());
    std::smatch sm;
    if (std::regex_match(deviceNameStd, sm, e)) {
        if (deviceVersion != nullptr) {
            *deviceVersion = sm[1];
        }
        if (cameraId != nullptr) {
            *cameraId = sm[2];
        }
        return true;
    }
    return false;
}

int getCameraDeviceVersion(const hidl_string &deviceName,
                           const hidl_string &providerType)
{
    std::string version;
    bool match = matchDeviceName(deviceName, providerType, &version, nullptr);
    if (!match) {
        return -1;
    }

    if (version.compare(kHAL3_4) == 0) {
        return CAMERA_DEVICE_API_VERSION_3_4_1;
    } else if (version.compare(kHAL3_3) == 0) {
        return CAMERA_DEVICE_API_VERSION_3_3_1;
    } else if (version.compare(kHAL3_2) == 0) {
        return CAMERA_DEVICE_API_VERSION_3_2_1;
    } else if (version.compare(kHAL1_0) == 0) {
        return CAMERA_DEVICE_API_VERSION_1_0_1;
    }
    return 0;
}

bool parseProviderName(const std::string &name, std::string *type /*out*/,
                       uint32_t *id /*out*/)
{
    if (!type || !id) {
        ALOGE("%s，%d", __FUNCTION__, __LINE__);
        return false;
    }

    std::string::size_type slashIdx = name.find('/');
    if (slashIdx == std::string::npos || slashIdx == name.size() - 1) {
        ALOGE("Provider name does not have separator between type and id %s，%d", __FUNCTION__, __LINE__);
        return false;
    }

    std::string typeVal = name.substr(0, slashIdx);

    char *endPtr;
    errno = 0;
    long idVal = strtol(name.c_str() + slashIdx + 1, &endPtr, 10);
    if (errno != 0) {
        ALOGE("%s，%d cannot parse provider id as an integer: %s", __FUNCTION__, __LINE__, name.c_str());
        return false;
    }
    if (endPtr != name.c_str() + name.size()) {
        ALOGE("%s，%d provider id has unexpected length: %s", __FUNCTION__, __LINE__, name.c_str());
        return false;
    }
    if (idVal < 0) {
        ALOGE("%s，%d id is negative:: %s,%d", __FUNCTION__, __LINE__, name.c_str(), static_cast<uint32_t>(idVal));
        return false;
    }

    *type = typeVal;
    *id = static_cast<uint32_t>(idVal);

    return true;
}

}

class NativeCameraHidl
{
    hidl_vec<hidl_string> getCameraDeviceNames(int g_camera_id, sp<ICameraProvider> provider);

    struct EmptyDeviceCb : public ICameraDeviceCallback {
        virtual Return<void> processCaptureResult(
            const hidl_vec<CaptureResult> & /*results*/) override {
            ALOGI("processCaptureResult callback");
            return Void();
        }

        virtual Return<void> notify(const hidl_vec<NotifyMsg> & /*msgs*/) override {
            ALOGI("notify callback");
            return Void();
        }
    };

    struct DeviceCb : public V3_4::ICameraDeviceCallback {
        DeviceCb(NativeCameraHidl *parent) : mParent(parent) {}
        Return<void> processCaptureResult_3_4(
            const hidl_vec<V3_4::CaptureResult> &results) override;
        Return<void> processCaptureResult(const hidl_vec<CaptureResult> &results) override;
        Return<void> notify(const hidl_vec<NotifyMsg> &msgs) override;

        void getInflightBufferKeys(std::vector<std::pair<int32_t, int32_t>> *out);
        android::status_t pushInflightBufferLocked(int32_t frameNumber, int32_t streamId, buffer_handle_t *buffer, int acquireFence) ;
        android::status_t popInflightBuffer(int32_t frameNumber, int32_t streamId,/*out*/ buffer_handle_t **buffer);
        void wrapAsHidlRequest(camera3_capture_request_t *request,/*out*/device::V3_2::CaptureRequest *captureRequest,/*out*/std::vector<native_handle_t *> *handlesCreated) ;


    private:
        bool processCaptureResultLocked(const CaptureResult &results);

        NativeCameraHidl *mParent;               // Parent object
        std::mutex mInflightLock;
    public:
        int mBufferId ;
    };

    struct TorchProviderCb : public ICameraProviderCallback {
        TorchProviderCb(NativeCameraHidl *parent) : mParent(parent) {}
        virtual Return<void> cameraDeviceStatusChange(
            const hidl_string &, CameraDeviceStatus) override {
            return Void();
        }

        virtual Return<void> torchModeStatusChange(
            const hidl_string &, TorchModeStatus newStatus) override {
            std::lock_guard<std::mutex> l(mParent->mTorchLock);
            mParent->mTorchStatus = newStatus;
            mParent->mTorchCond.notify_one();
            return Void();
        }

    private:
        NativeCameraHidl *mParent;               // Parent object
    };

    void castSession(const sp<ICameraDeviceSession> &session, int32_t deviceVersion,
                     sp<device::V3_3::ICameraDeviceSession> *session3_3 /*out*/,
                     sp<device::V3_4::ICameraDeviceSession> *session3_4 /*out*/);
    void createStreamConfiguration(const ::android::hardware::hidl_vec<V3_2::Stream> &streams3_2,
                                   StreamConfigurationMode configMode,
                                   ::android::hardware::camera::device::V3_2::StreamConfiguration *config3_2,
                                   ::android::hardware::camera::device::V3_4::StreamConfiguration *config3_4);

    void configurePreviewStream(const std::string &name,  int32_t deviceVersion,
                                sp<ICameraProvider> provider,
                                const AvailableStream *previewThreshold,
                                sp<ICameraDeviceSession> *session /*out*/,
                                V3_2::Stream *previewStream /*out*/,
                                HalStreamConfiguration *halStreamConfig /*out*/,
                                bool *supportsPartialResults /*out*/,
                                uint32_t *partialResultCount /*out*/);

    static Status getAvailableOutputStreams(camera_metadata_t *staticMeta,
                                            std::vector<AvailableStream> &outputStreams,
                                            const AvailableStream *threshold = nullptr);
public:
    int startnativePreview(int g_camera_id, int g_width, int g_height, int g_sensor_width, int g_sensor_height, int g_rotate);

    bool need_exit(void);
    bool exit_camera(void);
    int nativecamerainit(int g_rotate, int mirror);

protected:

    // In-flight queue for tracking completion of capture requests.
    struct InFlightRequest {
        // Set by notify() SHUTTER call.
        nsecs_t shutterTimestamp;

        bool errorCodeValid;
        ErrorCode errorCode;

        //Is partial result supported
        bool usePartialResult;

        //Partial result count expected
        uint32_t numPartialResults;

        // Message queue
        std::shared_ptr<ResultMetadataQueue> resultQueue;

        // Set by process_capture_result call with valid metadata
        bool haveResultMetadata;

        // Decremented by calls to process_capture_result with valid output
        // and input buffers
        ssize_t numBuffersLeft;

        // A 64bit integer to index the frame number associated with this result.
        int64_t frameNumber;

        // The partial result count (index) for this capture result.
        int32_t partialResultCount;

        // For buffer drop errors, the stream ID for the stream that lost a buffer.
        // Otherwise -1.
        int32_t errorStreamId;

        // If this request has any input buffer
        bool hasInputBuffer;

        // Result metadata
        ::android::hardware::camera::common::V1_0::helper::CameraMetadata collectedResult;

        // Buffers are added by process_capture_result when output buffers
        // return from HAL but framework.
        ::android::Vector<StreamBuffer> resultOutputBuffers;

        InFlightRequest() :
            shutterTimestamp(0),
            errorCodeValid(false),
            errorCode(ErrorCode::ERROR_BUFFER),
            usePartialResult(false),
            numPartialResults(0),
            resultQueue(nullptr),
            haveResultMetadata(false),
            numBuffersLeft(0),
            frameNumber(0),
            partialResultCount(0),
            errorStreamId(-1),
            hasInputBuffer(false) {}

        InFlightRequest(ssize_t numBuffers, bool hasInput,
                        bool partialResults, uint32_t partialCount,
                        std::shared_ptr<ResultMetadataQueue> queue = nullptr) :
            shutterTimestamp(0),
            errorCodeValid(false),
            errorCode(ErrorCode::ERROR_BUFFER),
            usePartialResult(partialResults),
            numPartialResults(partialCount),
            resultQueue(queue),
            haveResultMetadata(false),
            numBuffersLeft(numBuffers),
            frameNumber(0),
            partialResultCount(0),
            errorStreamId(-1),
            hasInputBuffer(hasInput) {}
    };

    // Map from frame number to the in-flight request state
    typedef ::android::KeyedVector<uint32_t, InFlightRequest *> InFlightMap;

    std::mutex mLock;                          // Synchronize access to member variables

    std::condition_variable mResultCondition;  // Condition variable for incoming results
    InFlightMap mInflightMap;                  // Map of all inflight requests

    std::mutex mTorchLock;                     // Synchronize access to torch status
    std::condition_variable mTorchCond;        // Condition variable for torch status
    TorchModeStatus mTorchStatus;              // Current torch status

    // Camera provider service
    sp<ICameraProvider> mProvider;
    // Camera provider type.
    std::string mProviderType;


    int g_surface_f = HAL_PIXEL_FORMAT_YCrCb_420_SP;

    sp<ANativeWindow> nativeWindow = NULL;
};

bool g_need_exit = false;

void IntSignalHandler(int signum, siginfo_t *, void *sigcontext)
{
    UNUSED(sigcontext);

    if (signum == SIGINT) {
        ALOGI("\nCTRL^C pressed");
        g_need_exit = true;

    } else if (signum == SIGTERM) {
        ALOGI("\n get Kill command");
        g_need_exit = true;
    }
}
bool NativeCameraHidl::need_exit(void)
{
    //todo:  check kernel if need exit camera
    return false;
}
bool NativeCameraHidl::exit_camera(void)
{
    return g_need_exit || need_exit();
}

int NativeCameraHidl::nativecamerainit(int g_rotate, int mirror)
{
    if (g_rotate == 90) {
        g_rotate  = NATIVE_WINDOW_TRANSFORM_ROT_90;
    } else if (g_rotate == 180) {
        g_rotate  = NATIVE_WINDOW_TRANSFORM_ROT_180;
    } else if (g_rotate == 270) {
        g_rotate  = NATIVE_WINDOW_TRANSFORM_ROT_270;
    } else {
        g_rotate  = 0;
    }

    if (mirror == 1) {
        g_rotate |= NATIVE_WINDOW_TRANSFORM_FLIP_H;
    } else {
        // Nothing to do;
    }
    struct sigaction act, oldact;
    memset(&act, 0, sizeof(act));
    act.sa_sigaction = ::IntSignalHandler;
    act.sa_flags = SA_RESTART | SA_SIGINFO | SA_ONSTACK;
    sigemptyset(&act.sa_mask);
    if (sigaction(SIGINT, &act, &oldact) != 0) {
        ALOGI("install signal error");
        return -1;
    }

    if (sigaction(SIGTERM, &act, &oldact) != 0) {
        ALOGI("install signal SIGTERM error");
        return -1;
    }
    return 0;
}

Return<void> NativeCameraHidl::DeviceCb::processCaptureResult_3_4(
    const hidl_vec<V3_4::CaptureResult> &results)
{
    if (nullptr == mParent) {
        return Void();
    }

    bool notify = false;
    std::unique_lock<std::mutex> l(mParent->mLock);
    for (size_t i = 0 ; i < results.size(); i++) {
        notify = processCaptureResultLocked(results[i].v3_2);
    }

    l.unlock();
    if (notify) {
        mParent->mResultCondition.notify_one();
    }

    return Void();
}

Return<void> NativeCameraHidl::DeviceCb::processCaptureResult(
    const hidl_vec<CaptureResult> &results)
{
    //ALOGI("debug_zy begin %s ,%d,frame_number=%d partialResult=%d,%p", __FUNCTION__,__LINE__,results[0].frameNumber, results[0].partialResult,&(results[0].outputBuffers[0].buffer));

    if (nullptr == mParent) {
        return Void();
    }

    bool notify = false;
    std::unique_lock<std::mutex> l(mParent->mLock);

    //ALOGI("debug_zy %s ,%d,results.size()=%zu", __FUNCTION__,__LINE__,results.size());
    for (size_t i = 0 ; i < results.size(); i++) {
        notify = processCaptureResultLocked(results[i]);
    }

    l.unlock();
    if (notify) {
        mParent->mResultCondition.notify_one();
    }

    return Void();
}
std::unordered_map<uint64_t, std::pair<buffer_handle_t *, int>> mInflightBufferMap;

void NativeCameraHidl::DeviceCb::getInflightBufferKeys(
    std::vector<std::pair<int32_t, int32_t>> *out)
{
    std::unique_lock<std::mutex> l(mParent->mLock);
    out->clear();
    out->reserve(mInflightBufferMap.size());
    for (auto & pair : mInflightBufferMap) {
        uint64_t key = pair.first;
        int32_t streamId = key & 0xFFFFFFFF;
        int32_t frameNumber = (key >> 32) & 0xFFFFFFFF;
        out->push_back(std::make_pair(frameNumber, streamId));
    }
    l.unlock();
    return;
}

android::status_t NativeCameraHidl::DeviceCb::pushInflightBufferLocked(
    int32_t frameNumber, int32_t streamId, buffer_handle_t *buffer, int acquireFence)
{
    uint64_t key = static_cast<uint64_t>(frameNumber) << 32 | static_cast<uint64_t>(streamId);
    auto pair = std::make_pair(buffer, acquireFence);
    mInflightBufferMap[key] = pair;
    return android::OK;
}

android::status_t NativeCameraHidl::DeviceCb::popInflightBuffer(
    int32_t frameNumber, int32_t streamId,
    /*out*/ buffer_handle_t **buffer)
{
    std::lock_guard<std::mutex> lock(mInflightLock);

    uint64_t key = static_cast<uint64_t>(frameNumber) << 32 | static_cast<uint64_t>(streamId);

    //ALOGI("debug_zy_map:%s，%d", __FUNCTION__,__LINE__);
    auto it = mInflightBufferMap.find(key);
    if (it == mInflightBufferMap.end()) {
        return android::NAME_NOT_FOUND;
    }
    auto pair = it->second;
    *buffer = pair.first;
    int acquireFence = pair.second;
    if (acquireFence > 0) {
        ::close(acquireFence);
    }
    mInflightBufferMap.erase(it);

    return android::OK;
}
void NativeCameraHidl::DeviceCb::wrapAsHidlRequest(camera3_capture_request_t *request,
                                                   /*out*/device::V3_2::CaptureRequest *captureRequest,
                                                   /*out*/std::vector<native_handle_t *> *handlesCreated)
{

    ALOGV("%s: captureRequest (%p) and handlesCreated (%p) ",
          __FUNCTION__, captureRequest, handlesCreated);

    if (captureRequest == nullptr || handlesCreated == nullptr) {
        ALOGE("%s: captureRequest (%p) and handlesCreated (%p) must not be null",
              __FUNCTION__, captureRequest, handlesCreated);
        return;
    }

    captureRequest->frameNumber = request->frame_number;
    //ALOGI("debug_zy :%s，%d,request->frame_number:%d", __FUNCTION__,__LINE__,request->frame_number);

    captureRequest->fmqSettingsSize = 0;

    {
        std::lock_guard<std::mutex> lock(mInflightLock);

        captureRequest->inputBuffer.streamId = -1;
        captureRequest->inputBuffer.bufferId = 0;

        //ALOGI("debug_zy :%s，%d,request->num_output_buffers:%d", __FUNCTION__,__LINE__,request->num_output_buffers);

        captureRequest->outputBuffers.resize(request->num_output_buffers);

        for (size_t i = 0; i < request->num_output_buffers; i++) {
            const camera3_stream_buffer_t *src = request->output_buffers + i;
            StreamBuffer &dst = captureRequest->outputBuffers[i];
            int32_t streamId = 0;
            buffer_handle_t buf = *(src->buffer);
            //auto pair = getBufferId(buf, streamId);
            bool isNewBuffer = 1;//pair.first;
            dst.streamId = streamId;
            if (mBufferId == 6) {
                mBufferId = 1;
            }

            //D("mBufferId:%d",mBufferId); //
            dst.bufferId = mBufferId++;//1;//pair.second;1 2 3 4 5
            dst.buffer = isNewBuffer ? buf : nullptr;

            //D(" buffer_handle:%d",(*request->output_buffers->buffer)->version); //12
            //	ALOGI("buffer_handle:%d",captureRequest->outputBuffers[0].buffer->version);

            dst.status = BufferStatus::OK;
            native_handle_t *acquireFence = nullptr;
            if (src->acquire_fence != -1) {
                acquireFence = native_handle_create(1, 0);
                acquireFence->data[0] = src->acquire_fence;
                handlesCreated->push_back(acquireFence);
            }
            dst.acquireFence = acquireFence;
            dst.releaseFence = nullptr;

            pushInflightBufferLocked(captureRequest->frameNumber, streamId,
                                     src->buffer, src->acquire_fence);
        }
    }
}

bool NativeCameraHidl::DeviceCb::processCaptureResultLocked(const CaptureResult &results)
{
    bool notify = false;
    uint32_t frameNumber = results.frameNumber;
    camera3_capture_result r;
    r.frame_number = results.frameNumber;
    android::status_t res;

    if ((results.result.size() == 0) &&
        (results.outputBuffers.size() == 0) &&
        (results.inputBuffer.buffer == nullptr) &&
        (results.fmqResultSize == 0)) {
        ALOGE("%s: No result data provided by HAL for frame %d result count: %d",
              __FUNCTION__, frameNumber, (int) results.fmqResultSize);
        return notify;
    }

    ssize_t idx = mParent->mInflightMap.indexOfKey(frameNumber);

    //ALOGI("debug_zy %s ,%d,idx:%zu,frameNumber:%d", __FUNCTION__,__LINE__,idx,frameNumber);
    if (::android::NAME_NOT_FOUND == idx) {
        ALOGE("%s: Unexpected frame number! received: %u",
              __FUNCTION__, frameNumber);
        return notify;
    }

    bool isPartialResult = false;
    bool hasInputBufferInRequest = false;
    InFlightRequest *request = mParent->mInflightMap.editValueAt(idx);
    ::android::hardware::camera::device::V3_2::CameraMetadata resultMetadata;
    size_t resultSize = 0;
    if (results.fmqResultSize > 0) {
        resultMetadata.resize(results.fmqResultSize);
        if (request->resultQueue == nullptr) {
            return notify;
        }
        if (!request->resultQueue->read(resultMetadata.data(),
                                        results.fmqResultSize)) {
            ALOGE("%s: Frame %d: Cannot read camera metadata from fmq,"
                  "size = %" PRIu64, __FUNCTION__, frameNumber,
                  results.fmqResultSize);
            return notify;
        }
        resultSize = resultMetadata.size();
    } else if (results.result.size() > 0) {
        resultMetadata.setToExternal(const_cast<uint8_t *>(
                                         results.result.data()), results.result.size());
        resultSize = resultMetadata.size();
    }

    if (!request->usePartialResult && (resultSize > 0) &&
        (results.partialResult != 1)) {
        ALOGE("%s: Result is malformed for frame %d: partial_result %u "
              "must be 1  if partial result is not supported", __FUNCTION__,
              frameNumber, results.partialResult);
        return notify;
    }

    if (results.partialResult != 0) {
        request->partialResultCount = results.partialResult;
    }

    // Check if this result carries only partial metadata
    if (request->usePartialResult && (resultSize > 0)) {
        if ((results.partialResult > request->numPartialResults) ||
            (results.partialResult < 1)) {
            ALOGE("%s: Result is malformed for frame %d: partial_result %u"
                  " must be  in the range of [1, %d] when metadata is "
                  "included in the result", __FUNCTION__, frameNumber,
                  results.partialResult, request->numPartialResults);
            return notify;
        }
        request->collectedResult.append(
            reinterpret_cast<const camera_metadata_t *>(
                resultMetadata.data()));

        isPartialResult =
            (results.partialResult < request->numPartialResults);
    } else if (resultSize > 0) {
        request->collectedResult.append(reinterpret_cast<const camera_metadata_t *>(
                                            resultMetadata.data()));
        isPartialResult = false;
    }

    hasInputBufferInRequest = request->hasInputBuffer;
    //ALOGI("debug_zy  %s ,%d,resultSize:%zu,isPartialResult:%d,haveResultMetadata:%d", __FUNCTION__,__LINE__,resultSize,isPartialResult,request->haveResultMetadata);
    // Did we get the (final) result metadata for this capture?
    if ((resultSize > 0) && !isPartialResult) {
        if (request->haveResultMetadata) {
            ALOGE("%s: Called multiple times with metadata for frame %d",
                  __FUNCTION__, frameNumber);
            //return notify;
        }
        request->haveResultMetadata = true;
        request->collectedResult.sort();
    }

    uint32_t numBuffersReturned = results.outputBuffers.size();

    std::vector<camera3_stream_buffer_t> outputBuffers(results.outputBuffers.size());
    std::vector<buffer_handle_t> outputBufferHandles(results.outputBuffers.size());
    for (size_t i = 0; i < results.outputBuffers.size(); i++) {
        auto &bDst = outputBuffers[i];
        const StreamBuffer &bSrc = results.outputBuffers[i];
        buffer_handle_t *buffer;
        res = popInflightBuffer(results.frameNumber, bSrc.streamId, &buffer);
        if (res != android::OK) {
            ALOGE("%s: Frame %d: Buffer %zu: No in-flight buffer for stream %d",
                  __FUNCTION__, results.frameNumber, i, bSrc.streamId);
            // return;
        }
        bDst.buffer = buffer;

        //D("queueBuffer");
        //ALOGI("debug_zy :%s，%d,version:%d", __FUNCTION__,__LINE__,(results.outputBuffers[0].buffer)->version);
        //ALOGI("debug_zy :%s，%d,version:%d,partialResult:%d", __FUNCTION__,__LINE__,(*outputBuffers[0].buffer)->version,results.partialResult);
#if 0//ok
        if (results.partialResult == 0) {
            android::GraphicBufferMapper &mapper = android::GraphicBufferMapper::get();
            void *dst;
            mapper.lock(*outputBuffers[0].buffer, GRALLOC_USAGE_SW_WRITE_OFTEN | GRALLOC_USAGE_SW_READ_OFTEN, android::Rect(1440, 1080), &dst);
            {
                char filename[128];
                sprintf(filename, "/data/vendor/cameraserver/%d_%dx%d.nv", results.frameNumber, g_sensor_width, g_sensor_height);
                FILE *fp = fopen(filename, "w+");
                if (fp) {
                    fwrite(dst, 1, g_sensor_height * g_sensor_width * 3 / 2, fp);
                    fclose(fp);
                }
            }

            mapper.unlock(*outputBuffers[0].buffer);
        }
#endif
        //	D("debug_zy_exit:%s，%d,%p", __FUNCTION__,__LINE__,outputBuffers[0].buffer);

        android::status_t res = mParent->nativeWindow->queueBuffer(mParent->nativeWindow.get(),
                                                                   container_of(outputBuffers[0].buffer, ANativeWindowBuffer, handle), -1);
        if (res != android::OK) {
            E("queueBuffer error=%d", res);
        }



    }
    r.num_output_buffers = outputBuffers.size();
    r.output_buffers = outputBuffers.data();



    if (results.inputBuffer.buffer != nullptr) {
        if (hasInputBufferInRequest) {
            numBuffersReturned += 1;
        } else {
            ALOGW("%s: Input buffer should be NULL if there is no input"
                  " buffer sent in the request", __FUNCTION__);
        }
    }

    //ALOGI("debug_zy  %s ,%d,numBuffersLeft:%zu,numBuffersReturned:%d", __FUNCTION__,__LINE__,request->numBuffersLeft,numBuffersReturned);
    request->numBuffersLeft -= numBuffersReturned;
    if (request->numBuffersLeft < 0) {
        ALOGE("%s: Too many buffers returned for frame %d", __FUNCTION__,
              frameNumber);
        // return notify;
    }
    request->resultOutputBuffers.appendArray(results.outputBuffers.data(),
                                             results.outputBuffers.size());
    // If shutter event is received notify the pending threads.
    if (request->shutterTimestamp != 0) {
        notify = true;
    }
    return notify;
}

Return<void> NativeCameraHidl::DeviceCb::notify(
    const hidl_vec<NotifyMsg> &messages)
{
    std::lock_guard<std::mutex> l(mParent->mLock);

    for (size_t i = 0; i < messages.size(); i++) {
        ssize_t idx = mParent->mInflightMap.indexOfKey(
                          messages[i].msg.shutter.frameNumber);

        //ALOGI("debug_zy %s ,%d,idx:%zu,frameNumber:%d", __FUNCTION__,__LINE__,idx,messages[i].msg.shutter.frameNumber);

        if (::android::NAME_NOT_FOUND == idx) {
            ALOGE("%s: Unexpected frame number! received: %u",
                  __FUNCTION__, messages[i].msg.shutter.frameNumber);
            break;
        }
        InFlightRequest *r = mParent->mInflightMap.editValueAt(idx);

        switch (messages[i].type) {
            case MsgType::ERROR:
                if (ErrorCode::ERROR_DEVICE == messages[i].msg.error.errorCode) {
                    ALOGE("%s: Camera reported serious device error",
                          __FUNCTION__);
                } else {
                    r->errorCodeValid = true;
                    r->errorCode = messages[i].msg.error.errorCode;
                    r->errorStreamId = messages[i].msg.error.errorStreamId;
                }
                break;
            case MsgType::SHUTTER:
                r->shutterTimestamp = messages[i].msg.shutter.timestamp;
                break;
            default:
                ALOGE("%s: Unsupported notify message %d", __FUNCTION__,
                      messages[i].type);
                break;
        }
    }

    mParent->mResultCondition.notify_one();
    return Void();
}

hidl_vec<hidl_string> NativeCameraHidl::getCameraDeviceNames(int g_camera_id, sp<ICameraProvider> provider)
{
    std::vector<std::string> cameraDeviceNames;
    Return<void> ret;
    ret = provider->getCameraIdList(
    [&](auto status, const auto & idList) {
        ALOGI("getCameraIdList returns status:%d", (int)status);
        for (size_t i = 0; i < idList.size(); i++) {
            ALOGI("Camera Id[%zu] is %s", i, idList[i].c_str());
        }
       // for (const auto & id : idList) {
          //  cameraDeviceNames.push_back(id);
        //}
        cameraDeviceNames.push_back(idList[g_camera_id].c_str());
    });
    if (!ret.isOk()) {
        ALOGE("%s，%d", __FUNCTION__, __LINE__);
    }

    // External camera devices are reported through cameraDeviceStatusChange
    struct ProviderCb : public ICameraProviderCallback {
        virtual Return<void> cameraDeviceStatusChange(
            const hidl_string &devName,
            CameraDeviceStatus newStatus) override {
            ALOGI("camera device status callback name %s, status %d",
                  devName.c_str(), (int) newStatus);
            if (newStatus == CameraDeviceStatus::PRESENT) {
                externalCameraDeviceNames.push_back(devName);

            }
            return Void();
        }

        virtual Return<void> torchModeStatusChange(
            const hidl_string &, TorchModeStatus) override {
            return Void();
        }

        std::vector<std::string> externalCameraDeviceNames;
    };
    sp<ProviderCb> cb = new ProviderCb;
    auto status = mProvider->setCallback(cb);

    for (const auto & devName : cb->externalCameraDeviceNames) {
        if (cameraDeviceNames.end() == std::find(
                cameraDeviceNames.begin(), cameraDeviceNames.end(), devName)) {
            cameraDeviceNames.push_back(devName);
        }
    }

    hidl_vec<hidl_string> retList(cameraDeviceNames.size());
    for (size_t i = 0; i < cameraDeviceNames.size(); i++) {
        retList[i] = cameraDeviceNames[i];
    }
    return retList;
}


// Retrieve all valid output stream resolutions from the camera
// static characteristics.
Status NativeCameraHidl::getAvailableOutputStreams(camera_metadata_t *staticMeta,
                                                   std::vector<AvailableStream> &outputStreams,
                                                   const AvailableStream *threshold)
{
    if (nullptr == staticMeta) {
        return Status::ILLEGAL_ARGUMENT;
    }

    camera_metadata_ro_entry entry;
    int rc = find_camera_metadata_ro_entry(staticMeta,
                                           ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, &entry);
    if ((0 != rc) || (0 != (entry.count % 4))) {
        return Status::ILLEGAL_ARGUMENT;
    }

    for (size_t i = 0; i < entry.count; i += 4) {
        if (ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_OUTPUT ==
            entry.data.i32[i + 3]) {
            if (nullptr == threshold) {
                AvailableStream s = {entry.data.i32[i + 1],
                                     entry.data.i32[i + 2], entry.data.i32[i]
                                    };
                outputStreams.push_back(s);
            } else {
                if ((threshold->format == entry.data.i32[i]) &&
                    (threshold->width >= entry.data.i32[i + 1]) &&
                    (threshold->height >= entry.data.i32[i + 2])) {
                    AvailableStream s = {entry.data.i32[i + 1],
                                         entry.data.i32[i + 2], threshold->format
                                        };
                    outputStreams.push_back(s);
                }
            }
        }

    }

    return Status::OK;
}

void NativeCameraHidl::createStreamConfiguration(
    const ::android::hardware::hidl_vec<V3_2::Stream> &streams3_2,
    StreamConfigurationMode configMode,
    ::android::hardware::camera::device::V3_2::StreamConfiguration *config3_2 /*out*/,
    ::android::hardware::camera::device::V3_4::StreamConfiguration *config3_4 /*out*/)
{
    ::android::hardware::hidl_vec<V3_4::Stream> streams3_4(streams3_2.size());
    size_t idx = 0;
    for (auto & stream3_2 : streams3_2) {
        V3_4::Stream stream;
        stream.v3_2 = stream3_2;
        streams3_4[idx++] = stream;
    }
    *config3_4 = {streams3_4, configMode, {}};
    *config3_2 = {streams3_2, configMode};
}

// Open a device session and configure a preview stream.
void NativeCameraHidl::configurePreviewStream(const std::string &name, int32_t deviceVersion,
                                              sp<ICameraProvider> provider,
                                              const AvailableStream *previewThreshold,
                                              sp<ICameraDeviceSession> *session /*out*/,
                                              V3_2::Stream *previewStream /*out*/,
                                              HalStreamConfiguration *halStreamConfig /*out*/,
                                              bool *supportsPartialResults /*out*/,
                                              uint32_t *partialResultCount /*out*/)
{

    std::vector<AvailableStream> outputPreviewStreams;
    ::android::sp<ICameraDevice> device3_x;
    ALOGI("configureStreams: Testing camera device %s", name.c_str());
    Return<void> ret;
    ret = provider->getCameraDeviceInterface_V3_x(
              name,
    [&](auto status, const auto & device) {
        ALOGI("getCameraDeviceInterface_V3_x returns status:%d",
              (int)status);
        device3_x = device;
    });

    sp<DeviceCb> cb = new DeviceCb(this);
    ret = device3_x->open(
              cb,
    [&](auto status, const auto & newSession) {
        ALOGI("device::open returns status:%d", (int)status);
        *session = newSession;
    });

    sp<device::V3_3::ICameraDeviceSession> session3_3;
    sp<device::V3_4::ICameraDeviceSession> session3_4;
    castSession(*session, deviceVersion, &session3_3, &session3_4);

    camera_metadata_t *staticMeta;
    ret = device3_x->getCameraCharacteristics([&](Status s,
    CameraMetadata metadata) {
        ALOGI("%s,%d,s: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(s));
        staticMeta = clone_camera_metadata(
                         reinterpret_cast<const camera_metadata_t *>(metadata.data()));
    });

    camera_metadata_ro_entry entry;
    auto status = find_camera_metadata_ro_entry(staticMeta,
                                                ANDROID_REQUEST_PARTIAL_RESULT_COUNT, &entry);
    if ((0 == status) && (entry.count > 0)) {
        *partialResultCount = entry.data.i32[0];
        *supportsPartialResults = (*partialResultCount > 1);
    }

    outputPreviewStreams.clear();
    auto rc = getAvailableOutputStreams(staticMeta,
                                        outputPreviewStreams, previewThreshold);
    ALOGI("%s,%d,rc: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(rc));
    free_camera_metadata(staticMeta);

    V3_2::Stream stream3_2 = {0, StreamType::OUTPUT,
                              static_cast<uint32_t>(outputPreviewStreams[0].width),
                              static_cast<uint32_t>(outputPreviewStreams[0].height),
                              static_cast<PixelFormat>(outputPreviewStreams[0].format),
                              GRALLOC1_CONSUMER_USAGE_HWCOMPOSER, 0, StreamRotation::ROTATION_0
                             };
    ::android::hardware::hidl_vec<V3_2::Stream> streams3_2 = {stream3_2};
    ::android::hardware::camera::device::V3_2::StreamConfiguration config3_2;
    ::android::hardware::camera::device::V3_4::StreamConfiguration config3_4;
    createStreamConfiguration(streams3_2, StreamConfigurationMode::NORMAL_MODE,
                              &config3_2, &config3_4);
    if (session3_4 != nullptr) {
        RequestTemplate reqTemplate = RequestTemplate::PREVIEW;
        ret = session3_4->constructDefaultRequestSettings(reqTemplate,
        [&config3_4](auto status, const auto & req) {
            ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(status));
            config3_4.sessionParams = req;
        });
        ret = session3_4->configureStreams_3_4(config3_4,
        [&](Status s, device::V3_4::HalStreamConfiguration halConfig) {
            ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(s));
            halStreamConfig->streams.resize(halConfig.streams.size());
            for (size_t i = 0; i < halConfig.streams.size(); i++) {
                halStreamConfig->streams[i] = halConfig.streams[i].v3_3.v3_2;
            }
        });
    } else if (session3_3 != nullptr) {
        ret = session3_3->configureStreams_3_3(config3_2,
        [&](Status s, device::V3_3::HalStreamConfiguration halConfig) {
            ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(s));
            halStreamConfig->streams.resize(halConfig.streams.size());
            for (size_t i = 0; i < halConfig.streams.size(); i++) {
                halStreamConfig->streams[i] = halConfig.streams[i].v3_2;
            }
        });
    } else {
        ret = (*session)->configureStreams(config3_2,
        [&](Status s, HalStreamConfiguration halConfig) {
            ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(s));
            *halStreamConfig = halConfig;
        });
    }
    *previewStream = stream3_2;
}

//Cast camera device session to corresponding version
void NativeCameraHidl::castSession(const sp<ICameraDeviceSession> &session, int32_t deviceVersion,
                                   sp<device::V3_3::ICameraDeviceSession> *session3_3 /*out*/,
                                   sp<device::V3_4::ICameraDeviceSession> *session3_4 /*out*/)
{

    switch (deviceVersion) {
        case CAMERA_DEVICE_API_VERSION_3_4_1: {
            auto castResult = device::V3_4::ICameraDeviceSession::castFrom(session);
            *session3_4 = castResult;
            break;
        }
        case CAMERA_DEVICE_API_VERSION_3_3_1: {
            auto castResult = device::V3_3::ICameraDeviceSession::castFrom(session);
            *session3_3 = castResult;
            break;
        }
        default:
            //no-op
            return;
    }
}
int NativeCameraHidl::startnativePreview(int g_camera_id, int g_width, int g_height, int g_sensor_width, int g_sensor_height, int g_rotate)
{
    ALOGI("%s: g_camera_id: %d", __FUNCTION__, g_camera_id);

    std::string service_name = "legacy/0";
    ALOGI("get service with name: %s", service_name.c_str());
    mProvider = ICameraProvider::getService(service_name);

    uint32_t id;
    parseProviderName(service_name, &mProviderType, &id);

    hidl_vec<hidl_string> cameraDeviceNames = getCameraDeviceNames(g_camera_id,mProvider);
    std::vector<AvailableStream> outputPreviewStreams;
    AvailableStream previewThreshold = {g_sensor_width, g_sensor_height,
                                        static_cast<int32_t>(PixelFormat::IMPLEMENTATION_DEFINED)
                                       };
    uint64_t bufferId = 0;
    uint32_t frameNumber = 0;
    ::android::hardware::hidl_vec<uint8_t> settings;

    for (const auto & name : cameraDeviceNames) {

        int deviceVersion = getCameraDeviceVersion(name, mProviderType);
        if (deviceVersion == CAMERA_DEVICE_API_VERSION_1_0_1) {
            continue;
        } else if (deviceVersion <= 0) {
            ALOGE("%s: Unsupported device version %d", __FUNCTION__, deviceVersion);
            return -1;
        }

        V3_2::Stream previewStream;
        HalStreamConfiguration halStreamConfig;
        sp<ICameraDeviceSession> session;
        bool supportsPartialResults = false;
        uint32_t partialResultCount = 0;
        configurePreviewStream(name, deviceVersion, mProvider, &previewThreshold, &session /*out*/,
                               &previewStream /*out*/, &halStreamConfig /*out*/,
                               &supportsPartialResults /*out*/,
                               &partialResultCount /*out*/);
        std::shared_ptr<ResultMetadataQueue> resultQueue;
        auto resultQueueRet =
            session->getCaptureResultMetadataQueue(
        [&resultQueue](const auto & descriptor) {
            resultQueue = std::make_shared<ResultMetadataQueue>(
                              descriptor);
            if (!resultQueue->isValid() ||
                resultQueue->availableToWrite() <= 0) {
                ALOGE("%s: HAL returns empty result metadata fmq,"
                      " not use it", __FUNCTION__);
                resultQueue = nullptr;
                // Don't use the queue onwards.
            }
        });

        InFlightRequest inflightReq = {1, false, supportsPartialResults,
                                       partialResultCount, resultQueue
                                      };
        RequestTemplate reqTemplate = RequestTemplate::PREVIEW;
        Return<void> ret;
        ret = session->constructDefaultRequestSettings(reqTemplate,
        [&](auto status, const auto & req) {
            ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(status));
            settings = req;
        });

#if 1
        // configure camera device
        int32_t _frameNum = 0;
        camera3_stream_t preview_stream;
        CameraMetadata	Metadata(5);
        camera3_stream_buffer_t sb;
        camera3_capture_request_t request_buf;

        // create preview surface
        int bufferIdx = 0;
        int maxConsumerBuffers;
        int max_buffers = 4; // can get from HAL
        android::DisplayInfo dinfo;

        ANativeWindowBuffer **anwBuffers = NULL;
        // create a client to surfaceflinger
        int trim_x = 0, trim_y = 0;
        sp<android::SurfaceComposerClient> composerClient = new android::SurfaceComposerClient();
        if (android::NO_ERROR != composerClient->initCheck()) {
            E("initCheck error");
            return -1;
        }

        //android::SurfaceComposerClient::getDisplayInfo(android::SurfaceComposerClient::getBuiltInDisplay(android::ISurfaceComposer::eDisplayIdMain), &dinfo);
        android::SurfaceComposerClient::getDisplayInfo(android::SurfaceComposerClient::getInternalDisplayToken(), &dinfo);
        E("w=%d,h=%d,xdpi=%f,ydpi=%f,fps=%f,ds=%f\n", dinfo.w, dinfo.h, dinfo.xdpi, dinfo.ydpi, dinfo.fps, dinfo.density);
        g_width = dinfo.w;
        g_height = dinfo.h;

        sp<android::SurfaceControl> surfaceControl = composerClient->createSurface(::android::String8("NativeCameraPreviewSurface"), g_width + trim_x, g_height + trim_y, g_surface_f);

        if (NULL == surfaceControl.get() || !surfaceControl->isValid()) {
            E("createSurface error");
            return -1;
        }
        android::SurfaceComposerClient::Transaction t;

        t.setPosition(surfaceControl, -trim_x / 2, 0);
        t.setLayer(surfaceControl, 0x7FFFFFFF).apply();
        t.show(surfaceControl);
        sp<Surface> surface = surfaceControl->getSurface();
        if (surface == NULL || surface.get() == NULL) {
            E("getSurface error");
            return -1;
        }

        nativeWindow = surface;
        android::status_t ui_status = native_window_api_connect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
        if (ui_status != android::OK) {
            E("native_window_api_connect error");
            //return -1;
        }
        //把窗口设置为隐藏应可以减少功耗
        ui_status = native_window_set_usage(nativeWindow.get(),
                                            GRALLOC_USAGE_SW_READ_OFTEN |
                                            GRALLOC_USAGE_SW_WRITE_OFTEN	|
                                            GRALLOC_USAGE_HW_TEXTURE |
                                            GRALLOC_USAGE_EXTERNAL_DISP);

        if (ui_status != android::OK) {
            E("native_window_set_usage error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }
        ui_status = native_window_set_scaling_mode(nativeWindow.get(), NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW);
        if (ui_status != android::OK) {
            E("native_window_set_scaling_mode error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }

        ui_status = native_window_set_buffers_dimensions(nativeWindow.get(), g_sensor_width, g_sensor_height);
        if (ui_status != android::OK) {
            E("native_window_set_buffers_dimensions error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }
        ui_status = native_window_set_buffers_format(nativeWindow.get(), g_surface_f);
        if (ui_status != android::OK) {
            E("native_window_set_buffers_format error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }

        ui_status = nativeWindow->query(nativeWindow.get(), NATIVE_WINDOW_MIN_UNDEQUEUED_BUFFERS, &maxConsumerBuffers);
        if (ui_status != android::OK) {
            E("Unable to query consumer undequeued");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }
        D("wants %d buffers", maxConsumerBuffers);
        int totalBuffers = maxConsumerBuffers + max_buffers;
        D("totalBuffers %d", totalBuffers);
        ui_status = native_window_set_buffer_count(nativeWindow.get(), totalBuffers);
        if (ui_status != android::OK) {
            E("native_window_set_buffer_count error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }
        D("rotate window");
        ui_status = native_window_set_buffers_transform(nativeWindow.get(), g_rotate | NATIVE_WINDOW_TRANSFORM_INVERSE_DISPLAY);
        if (ui_status != android::OK) {
            E("native_window_set_buffers_transform error");
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            return -1;
        }

        anwBuffers = new ANativeWindowBuffer*[totalBuffers];

        _frameNum = 0;
        bufferIdx = 0;

        hidl_handle buffer_handle ;

        sp<DeviceCb> cb = new DeviceCb(this);
        cb->mBufferId = 1 ;
        while (1) {
            //	D("dequeueBufferr");

            ui_status = native_window_dequeue_buffer_and_wait(nativeWindow.get(), &anwBuffers[bufferIdx]);
            if (exit_camera()) {
                break;
            }
            request_buf.frame_number		= _frameNum++;

            sb.stream = &preview_stream;
            sb.status = 0;
            sb.buffer = &anwBuffers[bufferIdx]->handle;
            sb.acquire_fence = -1;
            sb.release_fence = -1;

            //D("buffer_handle:%d",(*sb.buffer)->version);

            request_buf.output_buffers = (const camera3_stream_buffer_t *)&sb;
            request_buf.num_output_buffers = 1;

            //D("buffer_handle:%d",(*request_buf.output_buffers->buffer)->version); //12
            //D("debug_zy:%s，%d,%p", __FUNCTION__,__LINE__,sb.buffer);

            native_handle_t *buffers = (native_handle_t *)*sb.buffer ;
            buffer_handle = buffers;

            StreamBuffer outputBuffer = {halStreamConfig.streams[0].id,
                                         bufferId,
                                         buffer_handle,
                                         BufferStatus::OK,
                                         nullptr,
                                         nullptr
                                        };

            ::android::hardware::hidl_vec<StreamBuffer> outputBuffers = {outputBuffer};
            const StreamBuffer emptyInputBuffer = { -1, 0, nullptr,
                                                    BufferStatus::ERROR, nullptr, nullptr
                                                  };
            CaptureRequest request = {frameNumber, 0 /* fmqSettingsSize */, settings,
                                      emptyInputBuffer, outputBuffers
                                     };

            {
                std::unique_lock<std::mutex> l(mLock);
                inflightReq = {1, false, supportsPartialResults,
                               partialResultCount, resultQueue
                              };
                //	ALOGI("debug_zy %s ,%d,frameNumber:%d,shutterTimestamp:%" PRId64 ",haveResultMetadata:%d,numBuffersLeft:%zu", __FUNCTION__,__LINE__,frameNumber,inflightReq.shutterTimestamp,inflightReq.haveResultMetadata,inflightReq.numBuffersLeft);
                mInflightMap.add(frameNumber, &inflightReq);
            }
            CaptureRequest captureRequests_buf = {frameNumber++, 0 /* fmqSettingsSize */, settings,
                                                  emptyInputBuffer, outputBuffers
                                                 };

            std::vector<native_handle_t *> handlesCreated;
            cb->wrapAsHidlRequest(&request_buf, /*out*/&captureRequests_buf, /*out*/&handlesCreated);

            Status status = Status::INTERNAL_ERROR;
            uint32_t numRequestProcessed = 0;
            hidl_vec<BufferCache> cachesToRemove;

            ret = session->processCaptureRequest(
            {captureRequests_buf}, cachesToRemove, [&status, &numRequestProcessed](auto s,  //captureRequests {request}
            uint32_t n) {
                status = s;
                ALOGV("%s,%d,status: %d", __FUNCTION__, __LINE__, static_cast<uint32_t>(status));
                numRequestProcessed = n;
            });

            for (auto & handle : handlesCreated) {
                native_handle_delete(handle);
            }


            bufferIdx++;
            if (bufferIdx >= totalBuffers) {
                bufferIdx = 0;
            }
        }
#endif
        // Flush before waiting for request to complete.
        Return<Status> returnStatus = session->flush();

        {
            std::unique_lock<std::mutex> l(mLock);

            if (!inflightReq.errorCodeValid) {
            } else {
                switch (inflightReq.errorCode) {
                    case ErrorCode::ERROR_REQUEST:
                    case ErrorCode::ERROR_RESULT:
                    case ErrorCode::ERROR_BUFFER:
                        // Expected
                        break;
                    case ErrorCode::ERROR_DEVICE:
                    default:
                        ALOGE("Unexpected error:%d", static_cast<uint32_t>(inflightReq.errorCode));
                }
            }
            mInflightMap.clear();

            for (int i = 0; i < totalBuffers; i++) {
                //ALOGI("debug_zy totalBuffers:%d",totalBuffers);
                ui_status = nativeWindow->cancelBuffer(nativeWindow.get(), anwBuffers[i], -1);
                if (ui_status != android::OK) {
                    E("cancelBuffer error");
                }
            }
            native_window_api_disconnect(nativeWindow.get(), NATIVE_WINDOW_API_CAMERA);
            surface.clear();
            composerClient->dispose();
            delete []anwBuffers;

            ret = session->close();
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 6) {
        printf("%s id width height rotate mirror.   eg:nativecamera 0 1280 720 0 0\n", argv[0]);
        return 0;
    }

    int g_camera_id = 0; //back camera
    int g_width = 1440;
    int g_height = 1080;
//    int g_sensor_width = 1440;
//    int g_sensor_height = 1080;
    int g_rotate = 0;
    int mirror = 0;

    g_camera_id = atoi(argv[1]);
    g_sensor_width = atoi(argv[2]);
    g_sensor_height = atoi(argv[3]);
    g_rotate = atoi(argv[4]);
    mirror = atoi(argv[5]);

    int status = 0;
    NativeCameraHidl native_camera;

    status = native_camera.nativecamerainit(g_rotate, mirror);
    if (status != 0) {
        return -1;
    }

    status = native_camera.startnativePreview(g_camera_id, g_width, g_height, g_sensor_width, g_sensor_height, g_rotate);
    if (status != 0) {
        return -1;
    }

    return 0;
}
