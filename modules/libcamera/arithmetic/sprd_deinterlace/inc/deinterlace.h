#ifndef _DEINTERLACE_H_
#define _DEINTERLACE_H_

#include "type_defs.h"


#ifdef   __cplusplus
extern   "C"
{
#endif

int filter_slice_multi(int w, int h, uint8_t *prev_f, uint8_t *cur_f, uint8_t *next_f, uint8_t *out_f);
int init_filter_slice();
int exit_filter_slice();

typedef int (*FT_filter_slice)(int w, int h, uint8_t *prev_f, uint8_t *cur_f, uint8_t *next_f, uint8_t *out_f);
typedef int (*FT_filter_slice_multi)(int w, int h, uint8_t *prev_f, uint8_t *cur_f, uint8_t *next_f, uint8_t *out_f);
typedef	int (*FT_init_filter_slice)();
typedef	int (*FT_exit_filter_slice)();

#ifdef   __cplusplus
}
#endif


#endif //_DEINTERLACE_H_

