#ifndef _TYPE_DEFS_H_
#define _TYPE_DEFS_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>

#ifdef   __cplusplus
extern   "C"
{
#endif

typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef unsigned int uint32_t;
typedef signed int int32_t;

#if defined(__GNUC__)
typedef long long int64bit;
#define file_seek fseeko
#elif defined(_MSC_VER)
typedef __int64 int64bit;
#define file_seek _fseeki64
#endif

//#define PNULL NULL

#define FFMAX(a,b) ((a) > (b) ? (a) : (b))
#define FFMAX3(a,b,c) FFMAX(FFMAX(a,b),c)
#define FFMIN(a,b) ((a) > (b) ? (b) : (a))
#define FFMIN3(a,b,c) FFMIN(FFMIN(a,b),c)

#define FFSWAP(type,a,b) do{type SWAP_tmp= b; b= a; a= SWAP_tmp;}while(0)
#define FF_ARRAY_ELEMS(a) (sizeof(a) / sizeof((a)[0]))

#define FFABS(a) ((a) >= 0 ? (a) : (-(a)))
#define FFSIGN(a) ((a) > 0 ? 1 : -1)

#ifdef   __cplusplus
}
#endif


#endif //_TYPE_DEFS_H_
