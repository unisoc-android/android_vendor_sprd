package com.dream.camera;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.os.Handler;
import android.util.Log;

import com.android.camera.CameraActivity;
import com.android.camera.app.AppController;
import com.android.camera.settings.Keys;
import com.dream.camera.settings.DataConfig;
import com.dream.camera.settings.DataModuleBasic;
import com.dream.camera.settings.DataModuleManager;
import com.dream.camera.settings.DreamSettingUtil;
import com.android.camera2.R;
import com.dream.camera.ui.MakeUpButton;
import com.dream.camera.ui.MakeUpKey;
import com.dream.camera.ui.MakeUpLevel;
import com.android.camera.util.CameraUtil;

public class MakeupController implements View.OnClickListener, MakeUpLevel.MakeupLevelListener {

    public interface MakeupListener {

        public void onBeautyValueChanged(int[] value);

        public void onBeautyValueReset();

        public void setMakeUpController(MakeupController makeUpController);

        public void updateMakeLevel();
    }

    private class UpdateRunnable implements Runnable{
        @Override
        public void run() {
           Log.d(TAG,"beauty level change");
           mController.onBeautyValueChanged(getValue());
        }

    }

    private int[] getValue(){
        int[] curLevel = new int[mMakeUpParameterNum];
        curLevel[KEY_SKINSMOOTHLEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_SKIN_SMOOTH_LEVEL, mSkinSmoothDefaultValue));
        curLevel[KEY_SKINBRIGHTLEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_SKIN_BRIGHT_LEVEL, mSkinBrightDefaultValue));
        curLevel[KEY_LARGEEYELEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_ENLARGE_EYES_LEVEL, mEnlargeEyesDefaultValue));
        curLevel[KEY_SLIMFACELEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_SLIM_FACE_LEVEL, mSlimFaceDefaultValue));
        curLevel[KEY_SKINCOLORTYPE] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_SKIN_COLOR_TYPE, mSkinColorDefaultType));
        curLevel[KEY_SKINCOLORLEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_SKIN_COLOR_LEVEL, mSkinColorDefaultLevel));
        curLevel[KEY_LIPCOLORTYPE] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_LIPS_COLOR_TYPE, mLipsColorDefaultType));
        if (CameraUtil.isCameraBeautyAllFeatureEnabled()) {
            curLevel[KEY_LIPCOLORLEVEL] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_LIPS_COLOR_LEVEL, mLipsColorDefaultLevel));
            curLevel[KEY_REMOVEBLEMISHFLAG] = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3],Keys.KEY_MAKEUP_REMOVE_BLEMISH_LEVEL, mRemoveBlemishDefaultValue));
        } else {
            curLevel[KEY_REMOVEBLEMISHFLAG] = 0;
            curLevel[KEY_LIPCOLORLEVEL] = 0;
        }
        return curLevel;
    }

    // Bug 1018708 - hide or show BeautyButton on topPanel, and this is for ai beauty
    private int[] getDefaultBeautyValue() {
        int[] curLevel = new int[mMakeUpParameterNum];
        curLevel[KEY_SKINSMOOTHLEVEL] = DreamSettingUtil.convertToInt(mSkinSmoothDefaultValue);
        curLevel[KEY_SKINBRIGHTLEVEL] = DreamSettingUtil.convertToInt(mSkinBrightDefaultValue);
        curLevel[KEY_LARGEEYELEVEL] = DreamSettingUtil.convertToInt(mEnlargeEyesDefaultValue);
        curLevel[KEY_SLIMFACELEVEL] = DreamSettingUtil.convertToInt(mSlimFaceDefaultValue);
        curLevel[KEY_SKINCOLORTYPE] = DreamSettingUtil.convertToInt(mSkinColorDefaultType);
        curLevel[KEY_SKINCOLORLEVEL] = DreamSettingUtil.convertToInt(mSkinColorDefaultLevel);
        curLevel[KEY_LIPCOLORTYPE] = DreamSettingUtil.convertToInt(mLipsColorDefaultType);
        if (CameraUtil.isCameraBeautyAllFeatureEnabled()) {
            curLevel[KEY_LIPCOLORLEVEL] = DreamSettingUtil.convertToInt(mLipsColorDefaultLevelForAI);
            curLevel[KEY_REMOVEBLEMISHFLAG] = DreamSettingUtil.convertToInt(mRemoveBlemishDefaultValue);
        } else {
            curLevel[KEY_REMOVEBLEMISHFLAG] = 0;
            curLevel[KEY_LIPCOLORLEVEL] = 0;
        }
        return curLevel;
    }

    private void setValue(String key, int level){
        mCurDataModule.set(DataConfig.SettingStoragePosition.positionList[3], key,"" + level);
    }

    // front camera default values
    private final String F_SkIN_SMOOTH_DV = "6";
    private final String F_SkIN_BRIGTH_DV = "6";
    private final String F_SLIM_FACE_DV = "2";
    private final String F_ENLARGE_EYES_DV = "2";

    // back camera default values
    private final String B_SkIN_SMOOTH_DV = "6";
    private final String B_SkIN_BRIGTH_DV = "6";
    private final String B_SLIM_FACE_DV = "2";
    private final String B_ENLARGE_EYES_DV = "2";

    private final String TAG = "MakeupController";
    private String mSkinSmoothDefaultValue = B_SkIN_SMOOTH_DV;
    /**this is for arc beatuy lib **/
    private String mSkinBrightDefaultValue = B_SkIN_BRIGTH_DV;

    /**0:white, 1:rosy, 2:wheat, this is for sprd beatuy lib**/
    private final String mSkinColorDefaultType = "0";
    private final String mSkinColorDefaultLevel = "0";

    private String mEnlargeEyesDefaultValue = B_SLIM_FACE_DV;
    private String mSlimFaceDefaultValue = B_ENLARGE_EYES_DV;
    private final String mRemoveBlemishDefaultValue = "1";

    /**0:crimson, 1:pink, 2:fuchsia, this is for sprd beatuy lib**/
    private final String mLipsColorDefaultType = "0";
    private final String mLipsColorDefaultLevel = "4";

    private final String mLipsColorDefaultLevelForAI = "0";

    private static final int mMakeUpParameterNum = 9;
    private final int KEY_REMOVEBLEMISHFLAG = 0;
    private final int KEY_SKINSMOOTHLEVEL = 1;
    private final int KEY_SKINCOLORTYPE = 2;
    private final int KEY_SKINCOLORLEVEL = 3;
    private final int KEY_SKINBRIGHTLEVEL = 4;
    private final int KEY_LIPCOLORTYPE = 5;
    private final int KEY_LIPCOLORLEVEL = 6;
    private final int KEY_SLIMFACELEVEL = 7;
    private final int KEY_LARGEEYELEVEL = 8;

    private int[] mMakeUpParameter = new int[]{0,0,0,0,0,0,0,0,0};
    protected final CameraActivity mActivity;

    private LinearLayout mMakeupControllerView;
    private LinearLayout mMakeupBtPanel;
    private MakeUpLevel mSkinSmoothPanel;
    private MakeUpLevel mSkinBrightPanel;
    private MakeUpLevel mEnlargeEyesPanel;
    private MakeUpLevel mSlimFacePanel;
    private MakeUpLevel mRemoveBlemishPanel;
    private MakeUpLevel mSkinColorLevel;
    private MakeUpLevel mLipsColorLevel;
    private LinearLayout mSkinColorControlPanel;
    private LinearLayout mLipsColorControlPanel;

    private MakeUpButton mSkinSmoothBt;
    private MakeUpButton mRemoveBlemishBt;
    private MakeUpButton mSkinBrightBt;
    private MakeUpButton mSkinColorBt;
    private MakeUpButton mSkinColorWhiteBt;
    private MakeUpButton mSkinColorRosyBt;
    private MakeUpButton mSkinColorWheatBt;
    private MakeUpButton mEnlargeEyesBt;
    private MakeUpButton mSlimFaceBt;
    private MakeUpButton mLipsColorBt;
    private MakeUpButton mLipsColorCrimsonBt;
    private MakeUpButton mLipsColorPinkBt;
    private MakeUpButton mLipsColorFuchsiaBt;

    private MakeupListener mController;
    Handler mHandler;
    UpdateRunnable runnAble;
    private DataModuleBasic mCurDataModule;
    private boolean mFirstInit = false;

    public class ClickSubUIParam{
        // this class is for make up features which contains sub item
        // now , includes 2 features: "skinColor" & "lipsColor"
        int mCount; // count of sub items
        int mType;   // mMakeUpParameter[type]
        int mKeys[]; // keys array of sub items
        MakeUpButton mButtons[]; // buttons array of sub items
        int mDefaultIndex; // default index for which one is selected statue
        String mPrefTypeKey; // KEY_MAKEUP_SKIN_COLOR_TYPE

        ClickSubUIParam(int count , int type , int keys[] , MakeUpButton buttons[] , int defaultIndex , String perfTypeKey) {
            mCount = count;
            mType = type;
            mKeys = keys;
            mButtons = buttons;
            mDefaultIndex = defaultIndex;
            mPrefTypeKey = perfTypeKey;
        }

        public void setItemType() {
            for(int i = 0 ; i < mCount ; ++i) {
                if (mMakeUpParameter[mType] == mKeys[i]) {
                    if (mButtons[i] != null) {
                        mButtons[i].setSelect(true);
                    }
                    return;
                }
            }
            if(mButtons[mDefaultIndex] != null)
                mButtons[mDefaultIndex].setSelect(true);
            return;
        }

        boolean useThisFeature(View view) {
            for(int i = 0 ; i < mCount ; ++i) {
                if(mButtons[i] != null && view == mButtons[i])
                    return true;
            }
            return false;
        }
    }

    ClickSubUIParam skinColorSubItems = null;
    ClickSubUIParam lipsColorSubItems = null;

    private int mFeatureCounter = 0;
    private int skinColorTypeKeys[] = {MakeUpKey.SKIN_COLOR_TYPE_WHITE , MakeUpKey.SKIN_COLOR_TYPE_ROSY , MakeUpKey.SKIN_COLOR_TYPE_WHEAT};
    private int lipsColorTypeKeys[] = {MakeUpKey.LIPS_COLOR_TYPE_CRIMSON , MakeUpKey.LIPS_COLOR_TYPE_PINK , MakeUpKey.LIPS_COLOR_TYPE_FUCHSIA};

    private MakeUpButton skinColorButtons[] = {null , null , null};
    private MakeUpButton lipsColorButtons[] = {null , null , null};
    private MakeUpButton featureButtons[] = {null , null , null , null , null , null , null , null};
    // there is 1 element more than max feature amount.
    // the index 0 indicates the state is selected.

    public MakeupController(View extendPanelParent, MakeupListener listener, CameraActivity activity) {
        mActivity = activity;
        if (extendPanelParent != null) {
            mMakeupControllerView = (LinearLayout) extendPanelParent
                    .findViewById(R.id.dream_make_up_panel);
            mMakeupBtPanel= (LinearLayout) mMakeupControllerView.findViewById(R.id.make_up_bt_parent);

            mSkinSmoothPanel = (MakeUpLevel) mMakeupControllerView.findViewById(R.id.skin_smooth_panel);
            mSkinSmoothPanel.setKey(MakeUpKey.SKIN_SMOOTH_LEVEL);
            mSkinSmoothPanel.setListener(this);
            mSkinBrightPanel = (MakeUpLevel) mMakeupControllerView.findViewById(R.id.skin_bright_panel);
            mSkinBrightPanel.setKey(MakeUpKey.SKIN_BRIGHT_LEVEL);
            mSkinBrightPanel.setListener(this);
            mEnlargeEyesPanel = (MakeUpLevel)  mMakeupControllerView.findViewById(R.id.enlarge_eyes_panel);
            mEnlargeEyesPanel.setKey(MakeUpKey.LARGE_EYES_LEVEL);
            mEnlargeEyesPanel.setListener(this);
            mSlimFacePanel = (MakeUpLevel)  mMakeupControllerView.findViewById(R.id.slim_face_panel);
            mSlimFacePanel.setKey(MakeUpKey.SLIM_FACE_LEVEL);
            mSlimFacePanel.setListener(this);
            /* the below items only display when using sprd beauty lib, so add null pointer  protection*/
            mRemoveBlemishPanel = (MakeUpLevel) mMakeupControllerView.findViewById(R.id.remove_blemish_panel);
            if (mRemoveBlemishPanel != null) {
                mFirstInit = true;
                mRemoveBlemishPanel.setKey(MakeUpKey.REMOVE_BLEMISH_LEVEL);
                mRemoveBlemishPanel.setListener(this);
                mRemoveBlemishPanel.setMaxLevel(1);
                mRemoveBlemishPanel.setMinLevel(0);
            }

            mSkinColorLevel = (MakeUpLevel) mMakeupControllerView.findViewById(R.id.skincolor_level_panel);
            if (mSkinColorLevel != null) {
                mSkinColorLevel.setKey(MakeUpKey.SKIN_COLOR_LEVEL);
                mSkinColorLevel.setListener(this);
            }
            mSkinColorControlPanel = (LinearLayout) mMakeupControllerView.findViewById(R.id.skincolor_panel);
            if (mSkinColorControlPanel != null){
                mSkinColorControlPanel.setOnClickListener(this);
            }
            mLipsColorLevel =  (MakeUpLevel) mMakeupControllerView.findViewById(R.id.lipscolor_level_panel);
            if (mLipsColorLevel != null) {
                mLipsColorLevel.setKey(MakeUpKey.LIPS_COLOR_LEVEL);
                mLipsColorLevel.setListener(this);
            }
            mLipsColorControlPanel = (LinearLayout)  mMakeupControllerView.findViewById(R.id.lipscolor_panel);
            if (mLipsColorControlPanel != null) {
                mLipsColorControlPanel.setOnClickListener(this);
            }

            // below 4 features are always in-use
            mSkinSmoothBt = setButton(R.id.skin_smooth_btn , MakeUpKey.SKIN_SMOOTH , 1);
            mSkinBrightBt = setButton(R.id.skin_bright_btn , MakeUpKey.SKIN_BRIGHT , 1);
            mEnlargeEyesBt = setButton(R.id.enlarge_eyes_btn , MakeUpKey.LARGE_EYES , 1);
            mSlimFaceBt = setButton(R.id.slim_face_btn , MakeUpKey.SLIM_FACE , 1);

            // below 3 features are controlled by "persist.sys.cam.beauty.fullfuc (true/false)" together.
            mRemoveBlemishBt = setButton(R.id.remove_blemish_btn , MakeUpKey.REMOVE_BLEMISH , 1);

            mSkinColorBt = setButton(R.id.skin_color_btn , MakeUpKey.SKIN_COLOR , 1);
            if (mSkinColorBt != null) {
                mSkinColorWhiteBt = setButton(R.id.skincolor_white_btn, MakeUpKey.SKIN_COLOR_WHITE, 0);
                mSkinColorRosyBt = setButton(R.id.skincolor_rosy_btn, MakeUpKey.SKIN_COLOR_ROSY, 0);
                mSkinColorWheatBt = setButton(R.id.skincolor_wheat_btn, MakeUpKey.SKIN_COLOR_WHEAT, 0);
                skinColorButtons[0] = mSkinColorWhiteBt;
                skinColorButtons[1] = mSkinColorRosyBt;
                skinColorButtons[2] = mSkinColorWheatBt;
                skinColorSubItems = new ClickSubUIParam(3, KEY_SKINCOLORTYPE, skinColorTypeKeys, skinColorButtons, 0, Keys.KEY_MAKEUP_SKIN_COLOR_TYPE);
            }

            mLipsColorBt = setButton(R.id.red_lips_btn , MakeUpKey.LIPS_COLOR , 1);
            if (mLipsColorBt != null) {
                mLipsColorCrimsonBt = setButton(R.id.lipscolor_crimson_btn, MakeUpKey.LIPS_COLOR_CRIMSON, 0);
                mLipsColorPinkBt = setButton(R.id.lipscolor_pink_btn, MakeUpKey.LIPS_COLOR_PINK, 0);
                mLipsColorFuchsiaBt = setButton(R.id.lipscolor_fuchsia_btn, MakeUpKey.LIPS_COLOR_FUCHSIA, 0);
                lipsColorButtons[0] = mLipsColorCrimsonBt;
                lipsColorButtons[1] = mLipsColorPinkBt;
                lipsColorButtons[2] = mLipsColorFuchsiaBt;
                lipsColorSubItems = new ClickSubUIParam(3, KEY_LIPCOLORTYPE, lipsColorTypeKeys, lipsColorButtons, 0, Keys.KEY_MAKEUP_LIPS_COLOR_TYPE);
            }

            mController = listener;

            mHandler = new Handler(extendPanelParent.getContext().getMainLooper());
            runnAble = new UpdateRunnable();

            mController.setMakeUpController(this);
            mController.updateMakeLevel();
        }
    }

    // SPRD:Bug 839474. split to another 1 method for CCN optimization
    protected MakeUpButton setButton(int id , int key , int featureIncrase) {
        MakeUpButton button = (MakeUpButton)mMakeupControllerView.findViewById(id);
        if(button != null) {
            button.setOnClickListener(this);
            button.setButtonKey(key);
            if(featureIncrase > 0) {
                ++mFeatureCounter;
                featureButtons[mFeatureCounter] = button;
            }
        }
        return button;
    }

    private void initMakeupLevel() {
        int curLevel = 0;
        mCurDataModule = DataModuleManager.getInstance(mActivity).getCurrentDataModule();
        if(mCurDataModule.getDataSetting().mIsFront){
            mSkinSmoothDefaultValue = F_SkIN_SMOOTH_DV;
            mSkinBrightDefaultValue = F_SkIN_BRIGTH_DV;
            mSlimFaceDefaultValue = F_SLIM_FACE_DV;
            mEnlargeEyesDefaultValue = F_ENLARGE_EYES_DV;

        } else {
            mSkinSmoothDefaultValue = B_SkIN_SMOOTH_DV;
            mSkinBrightDefaultValue = B_SkIN_BRIGTH_DV;
            mSlimFaceDefaultValue = B_SLIM_FACE_DV;
            mEnlargeEyesDefaultValue = B_ENLARGE_EYES_DV;
        }
        if (mSkinSmoothPanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_SKIN_SMOOTH_LEVEL, "" + mSkinSmoothDefaultValue));
            mSkinSmoothPanel.setLevel(curLevel);
            mMakeUpParameter[KEY_SKINSMOOTHLEVEL] = curLevel;
        }
        if (mSkinBrightPanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_SKIN_BRIGHT_LEVEL, "" + mSkinBrightDefaultValue));
            mSkinBrightPanel.setLevel(curLevel);
            mMakeUpParameter[KEY_SKINBRIGHTLEVEL] = curLevel;
        }
        if (mEnlargeEyesPanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_ENLARGE_EYES_LEVEL, "" + mEnlargeEyesDefaultValue));
            mEnlargeEyesPanel.setLevel(curLevel);
            mMakeUpParameter[KEY_LARGEEYELEVEL] = curLevel;
        }
        if (mSlimFacePanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_SLIM_FACE_LEVEL, "" + mSlimFaceDefaultValue));
            mSlimFacePanel.setLevel(curLevel);
            mMakeUpParameter[KEY_SLIMFACELEVEL] = curLevel;
        }
        if (mRemoveBlemishPanel != null) {
            if (CameraUtil.isCameraBeautyAllFeatureEnabled()) {
                curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_REMOVE_BLEMISH_LEVEL, "" + mRemoveBlemishDefaultValue));
            } else {
                curLevel = 0;
            }
            mRemoveBlemishPanel.setLevel(curLevel);
            mMakeUpParameter[KEY_REMOVEBLEMISHFLAG] = curLevel;
        }
        if (mSkinColorLevel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_SKIN_COLOR_LEVEL, "" + mSkinColorDefaultLevel));
            mSkinColorLevel.setLevel(curLevel);
            mMakeUpParameter[KEY_SKINCOLORLEVEL] = curLevel;
        }
        if (mSkinColorControlPanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_SKIN_COLOR_TYPE, "" + mSkinColorDefaultType));
            setSkinColorType(curLevel);
            mMakeUpParameter[KEY_SKINCOLORTYPE] = curLevel;
        }
        if (mLipsColorLevel != null) {
            if (CameraUtil.isCameraBeautyAllFeatureEnabled()) {
                curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_LIPS_COLOR_LEVEL, "" + mLipsColorDefaultLevel));
            } else {
                curLevel = 0;
            }
            mLipsColorLevel.setLevel(curLevel);
            mMakeUpParameter[KEY_LIPCOLORLEVEL] = curLevel;
        }
        if (mLipsColorControlPanel != null) {
            curLevel = DreamSettingUtil.convertToInt(mCurDataModule.getString(DataConfig.SettingStoragePosition.positionList[3], Keys.KEY_MAKEUP_LIPS_COLOR_TYPE, "" + mLipsColorDefaultType));
            setLipsColorType(curLevel);
            mMakeUpParameter[KEY_LIPCOLORTYPE] = curLevel;
        }
        Log.d(TAG,"beauty level change");
        mController.onBeautyValueChanged(mMakeUpParameter);
    }

    // Bug 1018708 - hide or show BeautyButton on topPanel
    public void startBeautyWithoutController(boolean flag) {
        if (flag)
            mController.onBeautyValueChanged(getDefaultBeautyValue());
        else
            mController.onBeautyValueReset();
    }

    public void resumeMakeupControllerView() {
        Log.i(TAG, "resumeMakeupControllerView");
        if (mMakeupControllerView == null) {
            return;
        }
        mMakeupControllerView.setVisibility(View.VISIBLE);
        if (mMakeupBtPanel.getVisibility() == View.GONE) {
            resetAllIcon();
            hideAllSeekbar();
        }
        initMakeupLevel();
        mMakeupBtPanel.setVisibility(View.VISIBLE);
    }

    public void onlyInitMakeupLevel(){
        if (mMakeupControllerView == null) {
            return;
        }
        initMakeupLevel();
    }

    public void pauseMakeupControllerView() {
        Log.i(TAG, "pauseMakeupControllerView");
        if (mMakeupControllerView == null) {
            return;
        }
        mController.onBeautyValueReset();
        mMakeupBtPanel.setVisibility(View.GONE);
        mMakeupControllerView.setVisibility(View.GONE);
    }

    public void pauseMakeupControllerView(boolean resetValue) {
        Log.i(TAG, "pauseMakeupControllerView resetValue = " + resetValue);
        if (mMakeupControllerView == null) {
            return;
        }
        if (resetValue) {
            mController.onBeautyValueReset();
        }
        mMakeupBtPanel.setVisibility(View.GONE);
        mMakeupControllerView.setVisibility(View.GONE);
    }

    public void clickOneFeature(View view , MakeUpButton button , LinearLayout panel , ClickSubUIParam param) {
        // click on a LEVEL-1 feature's icon
        if (button != null) {
            int visible = View.VISIBLE;
            if(panel.getVisibility() == View.GONE)
                hideAllSeekbar();
            else if (panel.getVisibility() == View.VISIBLE)
                visible = View.GONE;

            panel.setVisibility(visible);
            changeFeatureIconUI(view , visible);
            if (param != null) {
                param.setItemType();
            }
        }
    }

    boolean clickOneItem(boolean skip , View view , ClickSubUIParam subItem , LinearLayout panel , MakeUpLevel level) {
        // click on a LEVEL-2 item's icon
        if (skip == true) return true;
        if (subItem.useThisFeature(view)) {
            panel.setVisibility(View.VISIBLE);
            level.setVisibility(View.VISIBLE);
            changeItemIconUI(view , subItem);
            Log.d(TAG,"beauty level change");
            mController.onBeautyValueChanged(getValue());
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View view) {
        // buttons of LEVEL - 1
        if(view == mSkinSmoothBt) {
            clickOneFeature(view , mSkinSmoothBt , mSkinSmoothPanel , null);
            featureButtons[0] = mSkinSmoothBt;
            return;
        } else if (view == mSkinBrightBt) {
            clickOneFeature(view , mSkinBrightBt , mSkinBrightPanel , null);
            featureButtons[0] = mSkinBrightBt;
            return;
        } else if (view == mEnlargeEyesBt) {
            clickOneFeature(view , mEnlargeEyesBt , mEnlargeEyesPanel , null);
            featureButtons[0] = mEnlargeEyesBt;
            return;
        } else if (view == mSlimFaceBt) {
            clickOneFeature(view , mSlimFaceBt , mSlimFacePanel , null);
            featureButtons[0] = mSlimFaceBt;
            return;
        } else if (view == mRemoveBlemishBt) {
            clickOneFeature(view , mRemoveBlemishBt , mRemoveBlemishPanel , null);
            featureButtons[0] = mRemoveBlemishBt;
            return;
        } else if (view == mSkinColorBt) {
            clickOneFeature(view , mSkinColorBt , mSkinColorControlPanel , skinColorSubItems);
            featureButtons[0] = mSkinColorBt;
            return;
        } else if (view == mLipsColorBt) {
            clickOneFeature(view , mLipsColorBt , mLipsColorControlPanel , lipsColorSubItems);
            featureButtons[0] = mLipsColorBt;
            return;
        }

        // buttons of LEVEL - 2
        boolean skip = false;
        skip = clickOneItem(skip , view , skinColorSubItems , mSkinColorControlPanel , mSkinColorLevel);
        clickOneItem(skip , view , lipsColorSubItems , mLipsColorControlPanel , mLipsColorLevel);
    }

    private void hideAllSeekbar() {
        if (mSkinSmoothPanel != null) {
            mSkinSmoothPanel.setVisibility(View.GONE);
        }
        if (mSkinBrightPanel != null) {
            mSkinBrightPanel.setVisibility(View.GONE);
        }
        if (mEnlargeEyesPanel != null) {
            mEnlargeEyesPanel.setVisibility(View.GONE);
        }
        if (mSlimFacePanel != null) {
            mSlimFacePanel.setVisibility(View.GONE);
        }
        if (mRemoveBlemishPanel != null) {
            mRemoveBlemishPanel.setVisibility(View.GONE);
        }
        if (mSkinColorControlPanel != null) {
            mSkinColorControlPanel.setVisibility(View.GONE);
        }
        if (mLipsColorControlPanel != null) {
            mLipsColorControlPanel.setVisibility(View.GONE);
        }
    }

    private void resetAllIcon() {
        int i = 0;
        // features
        for (i = 1 ; i <= mFeatureCounter ; ++i)
            featureButtons[i].setSelect(false);

        // lips items
        if(lipsColorSubItems != null) {
            for (i = 0 ; i < lipsColorSubItems.mCount ; ++i)
                lipsColorSubItems.mButtons[i].setSelect(false);
        }

        // skin items
        if(skinColorSubItems != null) {
            for (i = 0 ; i < skinColorSubItems.mCount ; ++i)
                skinColorSubItems.mButtons[i].setSelect(false);
        }
    }

    private void changeFeatureIconUI(View view , int visible) {
        // old , display as WHITE
        if(featureButtons[0] != null)
            featureButtons[0].setSelect(false);

        // new , display as YELLOW
        ((MakeUpButton)view).setSelect((visible == View.VISIBLE));
    }

    private void changeItemIconUI(View view , ClickSubUIParam subUIParam) {
        for(int i = 0 ; i < subUIParam.mCount ; ++i) {
            // set clicked button to display as YELLOW , and set indicate parameters
            if(subUIParam.mButtons[i] == view) {
                subUIParam.mButtons[i].setSelect(true);
                mCurDataModule.set(DataConfig.SettingStoragePosition.positionList[3], subUIParam.mPrefTypeKey,
                        "" + subUIParam.mKeys[i]);
                mMakeUpParameter[subUIParam.mType] = subUIParam.mKeys[i];
            }
            else // set no-clicked button to display as WHITE
                subUIParam.mButtons[i].setSelect(false);
        }
    }

    private void setSkinColorType(int colorType) {
        int curColorType = colorType;
        switch (curColorType) {
            case MakeUpKey.SKIN_COLOR_TYPE_WHITE: {
                if (mSkinColorWhiteBt != null) {
                    mSkinColorWhiteBt.setSelect(true);
                }
            }
            break;
            case MakeUpKey.SKIN_COLOR_TYPE_ROSY: {
                if (mSkinColorRosyBt != null) {
                    mSkinColorRosyBt.setSelect(true);
                }
            }
            break;
            case MakeUpKey.SKIN_COLOR_TYPE_WHEAT: {
                if (mSkinColorWheatBt != null) {
                    mSkinColorWheatBt.setSelect(true);
                }
            }
            break;
            default: {
                if (mSkinColorWhiteBt != null) {
                    mSkinColorWhiteBt.setSelect(true);
                }
            }
            break;
        }
    }

    private void setLipsColorType(int colorType) {
        int lipColorType = colorType;
        switch (lipColorType) {
            case MakeUpKey.LIPS_COLOR_TYPE_CRIMSON: {
                if (mLipsColorCrimsonBt != null) {
                    mLipsColorCrimsonBt.setSelect(true);
                }
            }
            break;
            case MakeUpKey.LIPS_COLOR_TYPE_PINK: {
                if (mLipsColorPinkBt != null) {
                    mLipsColorPinkBt.setSelect(true);
                }
            }
            break;
            case MakeUpKey.LIPS_COLOR_TYPE_FUCHSIA: {
                if (mLipsColorFuchsiaBt != null) {
                    mLipsColorFuchsiaBt.setSelect(true);
                }
            }
            break;
            default:{
                if (mLipsColorCrimsonBt != null) {
                    mLipsColorCrimsonBt.setSelect(true);
                }
            }
            break;
        }
    }

    @Override
    public void onBeautyLevelWillChanged(int key, int value) {
        int changeKey = key;
        int changeValue = value;
        int parameterKey = 0;
        switch (changeKey) {
            case MakeUpKey.SKIN_SMOOTH_LEVEL:
                parameterKey = KEY_SKINSMOOTHLEVEL;
                break;
            case MakeUpKey.REMOVE_BLEMISH_LEVEL:
                parameterKey = KEY_REMOVEBLEMISHFLAG;
                break;
            case MakeUpKey.SKIN_BRIGHT_LEVEL:
                parameterKey = KEY_SKINBRIGHTLEVEL;
                break;
            case MakeUpKey.SKIN_COLOR_LEVEL:
                parameterKey = KEY_SKINCOLORLEVEL;
                break;
            case MakeUpKey.LARGE_EYES_LEVEL:
                parameterKey = KEY_LARGEEYELEVEL;
                break;
            case MakeUpKey.SLIM_FACE_LEVEL:
                parameterKey = KEY_SLIMFACELEVEL;
                break;
            case MakeUpKey.LIPS_COLOR_LEVEL:
                parameterKey = KEY_LIPCOLORLEVEL;
                break;
            default:
                break;
        }
        if (changeKey == MakeUpKey.REMOVE_BLEMISH_LEVEL && mFirstInit) {
            mFirstInit = false;
            return;
        }
        mMakeUpParameter[parameterKey] = changeValue;
        Log.d(TAG,"beauty level will change");
        mHandler.removeCallbacks(runnAble);
        mHandler.postAtTime(runnAble, 100);
    }

    @Override
    public void onBeautyLevelChanged(int key, int value) {
        int changeKey = key;
        int changeValue = value;
        int parameterKey = 0;
        switch (changeKey) {
            case MakeUpKey.SKIN_SMOOTH_LEVEL:
                parameterKey = KEY_SKINSMOOTHLEVEL;
                break;
            case MakeUpKey.REMOVE_BLEMISH_LEVEL:
                parameterKey = KEY_REMOVEBLEMISHFLAG;
                break;
            case MakeUpKey.SKIN_BRIGHT_LEVEL:
                parameterKey = KEY_SKINBRIGHTLEVEL;
                break;
            case MakeUpKey.SKIN_COLOR_LEVEL:
                parameterKey = KEY_SKINCOLORLEVEL;
                break;
            case MakeUpKey.LARGE_EYES_LEVEL:
                parameterKey = KEY_LARGEEYELEVEL;
                break;
            case MakeUpKey.SLIM_FACE_LEVEL:
                parameterKey = KEY_SLIMFACELEVEL;
                break;
            case MakeUpKey.LIPS_COLOR_LEVEL:
                parameterKey = KEY_LIPCOLORLEVEL;
                break;
            default:
                break;
        }
        mMakeUpParameter[parameterKey] = changeValue;
        Log.d(TAG,"beauty level change");
        mController.onBeautyValueChanged(getValue());
    }
}
